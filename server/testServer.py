# coding=utf-8
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier
import json
import sys
import os
import hashlib
from decimal import *

# Load from your computer
#trainingPath = "/home/ege/blockchain/server/smalltrainx.csv"
trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainx.csv"
f = open(trainingPath)
x_train = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
    row = row[0].split(',')
    numrow = []
    for num in row:
        numrow.append(float(num))
    x_train.append(numrow)
f.close()

#trainingPath = "/home/ege/blockchain/server/smalltrainy.csv"
trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainy.csv"
f = open(trainingPath)
y_train = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
    y_train.append(float(row[0]))
f.close()

regr = linear_model.LinearRegression()

#print("x_train", x_train)
#print("y_train", y_train)

regr.fit(x_train, y_train)

# Make predictions using the testing set
model = regr.coef_.tolist()
print(model)


#pathTest = "/home/ege/blockchain/server/smalltestx.csv"
pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltestx.csv"
f = open(pathTest)
test = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
    row = row[0].split(',')
    numrow =[]
    for num in row:
        numrow.append(float(num))
    test.append(numrow)
f.close()

pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic public and private keys generated

x_test = []
for r in test:
    encrypted_r = []
    for p in r:
        encryptedP = (pk1.encrypt(p))
        encrypted_r.append(encryptedP)
    x_test.append(encrypted_r)

#print(x_test)
#results = regr.predict(x_test) #OverflowError: int too large to convert to float

encryptedModel = []
for i in model:
    iEncrypted = pk1.encrypt(i)
    encryptedModel.append(iEncrypted)
print(encryptedModel)


e10 = pk1.encrypt(10)
e100 = pk1.encrypt(100)
e110 = e10 + e100
print(sk1.decrypt(e110))
e220 = e110 * 2
print(sk1.decrypt(e220))




modelNew = [int(x*1) for x in model]
print("--", modelNew)
resultsTemp = np.dot(x_test, modelNew)
kkresult = pk1.encrypt(0)
for i in range(0, len(x_test[0])):
    kkresult = kkresult + modelNew[i]*x_test[0][i]

print(sk1.decrypt(kkresult))


print("**", resultsTemp)
for x in resultsTemp:
    print(x)
    print(sk1.decrypt(x))
print(plainResults)
