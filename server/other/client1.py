from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier

"""
1. Generate two pairs of keys. +
2. Read test data. +
3. Compute pk1(testData).
4. Send public keys and pk1(testData) back to the smart contract.
"""


def generate_keys():
	# RSA modulus length must be a multiple of 256 and >= 1024
	modulus_length = 256*32  # use larger value in production
	privatekey = RSA.generate(modulus_length, Random.new().read)
	publickey = privatekey.publickey()
	return privatekey, publickey


def encrypt_message(a_message, publickey):
	encrypted_msg = publickey.encrypt(a_message, 32)[0]
	# base64 encoded strings are database friendly
	encoded_encrypted_msg = base64.b64encode(encrypted_msg)
	return encoded_encrypted_msg


def decrypt_message(encoded_encrypted_msg, privatekey):
	decoded_encrypted_msg = base64.b64decode(encoded_encrypted_msg)
	decoded_decrypted_msg = privatekey.decrypt(decoded_encrypted_msg)
	return decoded_decrypted_msg

def read_csv(filename):
    with open(filename, newline='') as f_input:
        return [list(map(float, row)) for row in csv.reader(f_input)]



public_key, private_key = paillier.generate_paillier_keypair()

f = open('smalltest.csv')
test = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
	row = row[0].split(',')
	numrow =[]
	for num in row:
		numrow.append(float(num))
	test.append(numrow)
f.close()

encrypted_test = []
for r in test:
	encrypted_r = []
	for p in r:
		encrypted_r.append(public_key.encrypt(p))
	encrypted_test.append(encrypted_r)

decrypted_test = []
for r in encrypted_test:
	decrypted_r = []
	for e in r:
		decrypted_r.append(private_key.decrypt(e))
	decrypted_test.append(decrypted_r)

print("Original content: %s - (%d)" % (test, len(test)))
print("Encrypted message: %s - (%d)" % (encrypted_test, len(encrypted_test)))
print("Decrypted message: %s - (%d)" % (decrypted_test, len(decrypted_test)))


"""
privatekey1 , publickey1 = generate_keys()
#privatekey2 , publickey2 = generate_keys()

encrypted_msg = encrypt_message(test, publickey1)
decrypted_msg = decrypt_message(encrypted_msg, privatekey1).decode("utf-8")

print("Original content: %s - (%d)" % (test, len(test)))
print("Encrypted message: %s - (%d)" % (encrypted_msg, len(encrypted_msg)))
print("Decrypted message: %s - (%d)" % (decrypted_msg, len(decrypted_msg)))
"""


"""
public_key, private_key = paillier.generate_paillier_keypair()
secret_number_list = [int(1), int(2), int(3)]
encrypted_number_list = [public_key.encrypt(x) for x in secret_number_list]
decrypted_number_list = [private_key.decrypt(x) for x in encrypted_number_list]

print("Original content: %s - (%d)" % (secret_number_list, len(secret_number_list)))
print("Encrypted message: %s - (%d)" % (encrypted_number_list, len(encrypted_number_list)))
print("Decrypted message: %s - (%d)" % (decrypted_number_list, len(decrypted_number_list)))
"""

"""
a_message = "The quick brown fox jumped over the lazy dog"
privatekey , publickey = generate_keys()
encrypted_msg = encrypt_message(a_message.encode("utf-8"), publickey)
decrypted_msg = decrypt_message(encrypted_msg, privatekey).decode("utf-8")

#print("%s - (%d)" % (privatekey.exportKey() , len(privatekey.exportKey())))
#print("%s - (%d)" % (publickey.exportKey() , len(publickey.exportKey())))
print("Original content: %s - (%d)" % (a_message, len(a_message)))
print("Encrypted message: %s - (%d)" % (encrypted_msg, len(encrypted_msg)))
print("Decrypted message: %s - (%d)" % (decrypted_msg, len(decrypted_msg)))

assert(a_message == decrypted_msg)
print("Assertion succeeds.")
"""
