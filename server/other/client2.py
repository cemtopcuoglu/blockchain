# coding=utf-8
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier
import json
import sys
import os
import threading
import hashlib
import zmq

app = Flask(__name__)
CORS(app)
portNumber = sys.argv[2]
portNumberSelf = sys.argv[1]
host = sys.argv[3]
IP = "127.0.0.1"

def peerAPI():
    #    api.add_resource(peerConnect, '/peerConnect/')
    app.run(debug=False,host=str(host))

def receiveRequest():
    context = zmq.Context()
    HOST = str(IP+":"+str(portNumberSelf))
    php = "tcp://"+ HOST # how and where to connect
    s = context.socket(zmq.REP)
    s.bind(php)
    print(s.recv())
    s.send(b'message received')
    ## içinde

t1 = threading.Thread(target=receiveRequest, )
t2= threading.Thread(target=peerAPI, )

t1.start()
t2.start()

print("process started")
#path = (request.get_json())['path']
#print(path)

pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic public and private keys generated
    
enc_with_one_pub_key = {}
enc_with_one_pub_key['pk1'] = {'g': str(pk1.g),'n': str(pk1.n)}
    
pathTest = "/home/ege/blockchain/server/smalltestx.csv"
f = open(pathTest)
test = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
    row = row[0].split(',')
    numrow =[]
    for num in row:
        numrow.append(float(num))
    test.append(numrow)
f.close()

pathResult = pathTest = "/home/ege/blockchain/server/smalltesty.csv"
f = open(pathResult)
testResultsEncrpyted = []  ## encrypted not hashed test results
testResultsEncrpytedJson = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
    encryptedRow = pk1.encrypt(float(row[0]))
    testResultsEncrpytedJson.append((str(encryptedRow.ciphertext()), encryptedRow.exponent))
f.close()
#print("encrypted not hashed test results", testResultsEncrpytedJson)

testResultJSON = [] ## encrypted and hashed test results
for x in testResultsEncrpytedJson:
    h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
    hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
    testResultJSON.append((h, hExponent))
    
#print("encrypted and hashed test results", testResultJSON)
    
    
    # first encrypt test with a symmetric key, then use paillier?
    
testEncryptedJson = []
for r in test:
    encrypted_r = []
    for p in r:
        encryptedP = (pk1.encrypt(p))
        encrypted_r.append((str(encryptedP.ciphertext()), encryptedP.exponent))
    testEncryptedJson.append(encrypted_r)


testEncryptedHashedJSON = []
for i in testEncryptedJson:
    testjson_i = []
    for x in i:
        h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
        hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
        testjson_i.append((h, hExponent))
    testEncryptedHashedJSON.append(testjson_i)


## TODO: send to the smart contract

context = zmq.Context()
HOST = str(IP+":"+str(portNumber))
php = "tcp://"+ HOST
s = context.socket(zmq.REQ)
s.connect(php) # block until connected
s.send_string(json.dumps(testEncryptedJson)) # send message
message = s.recv().decode()
print("message", message)

    ##encrypted_test ## send to other guy

enc_with_one_pub_key['hashedTest'] = testEncryptedHashedJSON
enc_with_one_pub_key['hashedTestResults'] = testResultJSON

#    """enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in encrypted_test]
#        serialised = json.dumps(enc_with_one_pub_key)"""

#enc_with_one_pub_key['pk2'] = pk2json['pk2']
serialised = json.dumps(enc_with_one_pub_key)

print(serialised)
#return serialised #, pk2json

"""
/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server
"""