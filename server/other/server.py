# coding=utf-8
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier
import json

app = Flask(__name__)
CORS(app)

def generate_keys():
    # RSA modulus length must be a multiple of 256 and >= 1024
    modulus_length = 256*4  # use larger value in production
    privatekey = RSA.generate(modulus_length, Random.new().read)
    publickey = privatekey.publickey()
    return publickey, privatekey

def encrypt_message(a_message, publickey):
    encrypted_msg = publickey.encrypt(a_message, 32)[0]
    # base64 encoded strings are database friendly
    encoded_encrypted_msg = base64.b64encode(encrypted_msg)
    return encoded_encrypted_msg

def decrypt_message(encoded_encrypted_msg, privatekey):
    decoded_encrypted_msg = base64.b64decode(encoded_encrypted_msg)
    decoded_decrypted_msg = privatekey.decrypt(decoded_encrypted_msg)
    return decoded_decrypted_msg

@app.route("/")
def helloWorld():
    return jsonify("fee")

@app.route("/client", methods = ['POST'])
def client():
    path = (request.get_json())['path']
    print(path)
    
    pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic
    pk2, sk2 = generate_keys() # düz rsa

    enc_with_one_pub_key = {}
    enc_with_one_pub_key['pk1'] = {'g': str(pk1.g),'n': str(pk1.n)}
    
    f = open(path)
    test = []
    csv_reader = csv.reader(f, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        row = row[0].split(',')
        numrow =[]
        for num in row:
            numrow.append(float(num))
        test.append(numrow)
    f.close()

    # first encrypt test with a symmetric key, then use paillier?

    encrypted_test = []
    for r in test:
        encrypted_r = []
        for p in r:
            encrypted_r.append(pk1.encrypt(p))
        encrypted_test.append(encrypted_r)

    testjson = []
    for i in encrypted_test:
        testjson_i = []
        for x in i:
            testjson_i.append((str(x.ciphertext()), x.exponent))
        testjson.append(testjson_i)

    enc_with_one_pub_key['values'] = testjson
    
    """enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in encrypted_test]
    serialised = json.dumps(enc_with_one_pub_key)"""
    
    #  pk2string = pk2.export_key().decode('utf8').replace("'", '"').fi

    pk2json = {'pk2':pk2.exportKey().decode('utf8').replace("'", '"')}
    pk2json['pk2'] = pk2json['pk2'].replace('\n', '@')
    enc_with_one_pub_key['pk2'] = pk2json['pk2']
    serialised = json.dumps(enc_with_one_pub_key)

    print(serialised)
    return serialised #, pk2json

app.run(debug=True,host='127.0.0.1')

"""
/home/ege/blockchain/server/smalltest.csv
"""