import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from phe import paillier
from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA
from Crypto import Random

def generate_keys():
	# RSA modulus length must be a multiple of 256 and >= 1024
	modulus_length = 256*4  # use larger value in production
	privatekey = RSA.generate(modulus_length, Random.new().read)
	publickey = privatekey.publickey()
	return publickey, privatekey

# Read from the blockchain
testSet = ""
apk1str = ""
apk2str = ""
apk2 = "" # convert str into a key
# Load from your computer
trainingSet = ""

# Split the datasets into feature and target sets
x_train = trainingSet[:, :-1]
y_train = trainingSet[:, -1]
x_test = testSet[:, :-1] 
y_test = testSet[:, -1] ## bu yok?

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(x_train, y_train)

# Make predictions using the testing set
results = regr.predict(x_test)
model = regr.coef_

pk, sk = generate_keys()

# results is already encrypted
# compute apk2(pk(model))

pkmodel = [pk.encrypt(x) for x in model]
encrypted_model = [apk2.encrypt(x) for x in pkmodel]

# send encrypted_model and results to smart contract



"""
public_key, private_key = paillier.generate_paillier_keypair()
enc_model = [public_key.encrypt(x) for x in model]

h1 = SHA.new(results)
pk1 = RSA.importKey(pk1str)
cipher1 = PKCS1_v1_5.new(pk1)
enc_results = cipher.encrypt(results+h1.digest())

h2 = SHA.new(enc_model)
pk2 = RSA.importKey(pk2str)
cipher2 = PKCS1_v1_5.new(pk2)
db_enc_model = cipher.encrypt(enc_model+h2.digest())

#WRITE THESE STUFF TO TXT FILES

From Crypto.Hash import SHA
from Crypto import Random
key = RSA.importKey(open('privkey.der').read())

dsize = SHA.digest_size
# Let's assume that average data length is 15
sentinel = Random.new().read(15+dsize)

cipher = PKCS1_v1_5.new(key)
message = cipher.decrypt(ciphertext, sentinel)

digest = SHA.new(message[:-dsize]).digest()
# Note how we DO NOT look for the sentinel
if digest == message[-dsize:]:
     print "Encryption was correct."
 else:
     print "Encryption was not correct."
"""