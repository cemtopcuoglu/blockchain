from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier

"""
1. Generate two pairs of keys. +
2. Read test data. +
3. Compute pk1(testData). +
4. Send public keys and pk1(testData) back to the smart contract.
"""

def generate_keys():
	# RSA modulus length must be a multiple of 256 and >= 1024
	modulus_length = 256*4  # use larger value in production
	privatekey = RSA.generate(modulus_length, Random.new().read)
	publickey = privatekey.publickey()
	return publickey, privatekey


def encrypt_message(a_message, publickey):
	encrypted_msg = publickey.encrypt(a_message, 32)[0]
	# base64 encoded strings are database friendly
	encoded_encrypted_msg = base64.b64encode(encrypted_msg)
	return encoded_encrypted_msg


def decrypt_message(encoded_encrypted_msg, privatekey):
	decoded_encrypted_msg = base64.b64decode(encoded_encrypted_msg)
	decoded_decrypted_msg = privatekey.decrypt(decoded_encrypted_msg)
	return decoded_decrypted_msg

pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic
pk2, sk2 = generate_keys() # düz rsa

f = open('test.csv')
test = []
csv_reader = csv.reader(f, delimiter=';')
next(csv_reader)
for row in csv_reader:
	row = row[0].split(',')
	numrow =[]
	for num in row:
		numrow.append(float(num))
	test.append(numrow)
f.close()

# first encrypt test with a symmetric key, then use paillier?

encrypted_test = []
for r in test:
	encrypted_r = []
	for p in r:
		encrypted_r.append(pk1.encrypt(p))
	encrypted_test.append(encrypted_r)

"""
decrypted_test = []
for r in encrypted_test:
	decrypted_r = []
	for e in r:
		decrypted_r.append(sk1.decrypt(e))
	decrypted_test.append(decrypted_r)
"""
"""
print("Original content: %s - (%d)" % (test, len(test)))
print("Encrypted message: %s - (%d)" % (encrypted_test, len(encrypted_test)))
print("Decrypted message: %s - (%d)" % (decrypted_test, len(decrypted_test)))
"""

# TODO: send pk1, pk2, encrypted_test to smart contract