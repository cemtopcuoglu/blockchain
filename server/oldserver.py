# coding=utf-8
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Hash import SHA
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier
import json
import sys
import os
import threading
import hashlib
import zmq
from decimal import *

app = Flask(__name__)
CORS(app)
portNumber = sys.argv[2]
portNumberSelf = sys.argv[1]
host = sys.argv[3]
port = sys.argv[4]
IP = "127.0.0.1"
encryptedTest = []
encryptedResults = []
results = []
encryptedModel = []
model = []
hashedCoef = []
pk = ""
sk = ""
exponents = []
requestDataEventNumber = 0
sendResultEventNumber = 0
decideResultEventNumber = 0
sendModelEventNumber = 0

def peerAPI():
    #    api.add_resource(peerConnect, '/peerConnect/')
    app.run(debug=False,host=str(host), port=int(port))

def receiveRequest():
    global encryptedTest
    context = zmq.Context()
    HOST = str(IP+":"+str(portNumberSelf))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedTest = json.loads((s.recv()))
    s.send(b'message received')
    ## içinde
    
def receiveRequestResult():
    global encryptedResults
    context = zmq.Context()
    HOST = str(IP+":"+str(int(portNumberSelf)+1))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedResults = json.loads((s.recv()))
    s.send(b'message received')

def receiveModel():
    global encryptedModel
    context = zmq.Context()
    HOST = str(IP+":"+str(int(portNumberSelf)+2))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedModel = json.loads((s.recv()))
    s.send(b'message received')

@app.route("/")
def helloWorld():
    return jsonify("fee")


@app.route("/getModel", methods = ['POST'])
def getModel():
    data = (request.get_json())['data']
    global sendModelEventNumber
    incomingNumber = int(data['sendModelEventNumber'])
    if incomingNumber > sendModelEventNumber:
        sendModelEventNumber += 1
        global hashedCoef
        global encryptedModel
        global pk
        global sk
        print("received the model")
        fromAddress = data['from']
        toAddress = data['to']

        #TODO 2: fix decryption from serialized object
        #ValueError: invalid literal for int() with base 10: 't'
        # x[0] is a long string representation of an int
        # x[1] is a string representation of an int
        # but this doesnt work
        """print(pk)
        print(encryptedModel)
        encryptedModelTemp = encryptedModel
        encryptedModel = [paillier.EncryptedNumber(pk, int(x[0]), int(x[1])) for x in encryptedModelTemp]
        model = [sk.decrypt(x) for x in encryptedModel]

        print("hashing to check")
        modelHash = ""
        for i in model:
            modelHash = modelHash + hashlib.sha256(str(i).encode('utf-8')).hexdigest() + ","
        
        print(modelHash == hashedCoef)"""


        pathTest = "/home/ege/blockchain/server/smalltestx.csv"
        #pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltestx.csv"
        f = open(pathTest)
        test = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            row = row[0].split(',')
            numrow =[]
            for num in row:
                numrow.append(float(num))
            test.append(numrow)
        f.close()

        """
        computedResults = []
        for row in test:
            temp = 0
            for i in range(0, len(row)):
                temp += row[i] * model[i]
            computedResults.append(temp)
        """
        #computedResults = np.dot(test, model)
        computedResults = [20.1, 20.9, 21.3, 22.0, 17.7] # TODO 3: remove this once the first two are done

        todump = {}
        todump['dataOwner'] = fromAddress
        if computedResults == results:
            todump['decision'] = 1 # confirm
        else:
            todump['decision'] = 0 # reject

        return json.dumps(todump)
    
    else:
        todump = {}
        todump['dataOwner'] = "dummy"
        todump['decision'] = -1
        return json.dumps(todump)        

@app.route("/sendModel", methods = ['POST'])
def sendModel():
    data = (request.get_json())['data']
    global decideResultEventNumber
    incomingNumber = int(data['decideResultEventNumber'])
    if incomingNumber > decideResultEventNumber:
        decideResultEventNumber += 1
        global encryptedModel
        print("sending the model")
        fromAddress = data['from']
        toAddress = data['to']

        todump = {}
        todump['testOwner'] = fromAddress
        todump['encryptedModel'] = [(str(x.ciphertext()), x.exponent) for x in encryptedModel]
        
        context = zmq.Context()
        HOST = str(IP+":"+str(int(portNumber) + 2))
        php = "tcp://"+ HOST
        s = context.socket(zmq.REQ)
        s.connect(php) # block until connected
        s.send_string( json.dumps(todump)) # send message
        message = s.recv().decode()
        print("message", message)
        
        return json.dumps(todump)

    else:
        todump = {}
        todump['testOwner'] = "dummy"
        todump['encryptedModel'] = []
        return json.dumps(todump)

@app.route("/getResults", methods = ['POST'])
def getResults():
    data = (request.get_json())['data']
    global sendResultEventNumber
    incomingNumber = int(data['sendResultEventNumber'])
    if incomingNumber > sendResultEventNumber:
        sendResultEventNumber += 1
        global encryptedResults
        global hashedCoef
        global results
        print("accuracy check started")
        fromAddress = data['from']
        toAddress = data['to']
        coef = data['coef']
        results = data['results']   
        hashedCoef = coef

        print("hashing to check")
        encResultsHash = ""
        for i in encryptedResults:
            encResultsHash = encResultsHash + hashlib.sha256(str(i).encode('utf-8')).hexdigest() + ","
        
        print(encResultsHash == results)
        
        # TODO 1: fix this part (decryption yields overflow)
        """
        encryptedResultsDeneme = [paillier.EncryptedNumber(pk, x) for x in encryptedResults]
        plainResults = [sk.decrypt(x) for x in encryptedResultsDeneme]
        print(plainResults)
        """
        
        plainResults = [20.1, 20.9, 21.3, 22.0, 17.7] # dummy values
        results = plainResults

        realResults = []
        pathResult = pathTest = "/home/ege/blockchain/server/smalltesty.csv"
        #pathResult = pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltesty.csv"
        f = open(pathResult)
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            realResults.append(float(row[0]))
        f.close()

        print(realResults)
        print(plainResults)

        error = mean_squared_error(realResults, plainResults)

        print(error)

        errorLimit = 5
        todump = {}
        todump['dataOwner'] = fromAddress
        if error < errorLimit:
            todump['decision'] = 1 # confirm
        else:
            todump['decision'] = 0 # reject

        return json.dumps(todump)
        
    else:
        todump = {}
        todump['dataOwner'] = "dummy"
        todump['decision'] = -1
        return json.dumps(todump)

@app.route("/requestTraining", methods = ['POST'])
def requestTraining():
    data = (request.get_json())['data']
    global requestDataEventNumber
    incomingNumber = int(data['requestDataEventNumber'])
    if incomingNumber > requestDataEventNumber:
        requestDataEventNumber += 1
        global encryptedTest
        global encryptedModel
        global modelStored
        print("training process started")
        fromAddress = data['from']
        toAddress = data['to']
        row = data['row']
        col = data['col']
        testCipher = data['testCipher']
        testExp = data['testExp']
        testResultCipher = data['testResultCipher']
        testResultExp = data['testResultExp']
        pk1g = data['pk1g']
        pk1n = data['pk1n']
        
        print("hashing to check test data")
        encryptedTestHash = ""
        encryptedTestHashExp = ""
        for i in encryptedTest:
            for j in i:
                encryptedTestHash = encryptedTestHash + hashlib.sha256(str(j[0]).encode('utf-8')).hexdigest() + ","
                encryptedTestHashExp = encryptedTestHashExp + hashlib.sha256(str(j[1]).encode('utf-8')).hexdigest() + ","
        
        print(encryptedTestHash == testCipher)
        print(encryptedTestHashExp == testExp)

        testSet = []

        for i in encryptedTest:
            temp = []
            for j in i:
                temp.append(int(j[0]))
            testSet.append(temp)

        #print("testSet", testSet)

        # Load from your computer
        trainingPath = "/home/ege/blockchain/server/smalltrainx.csv"
        #trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainx.csv"
        f = open(trainingPath)
        x_train = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            row = row[0].split(',')
            numrow =[]
            for num in row:
                numrow.append(float(num))
            x_train.append(numrow)
        f.close()

        trainingPath = "/home/ege/blockchain/server/smalltrainy.csv"
        #trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainy.csv"
        f = open(trainingPath)
        y_train = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            y_train.append(float(row[0]))
        f.close()


        # Split the datasets into feature and target sets
        # x_train = trainingSet[:, :-1]
        # y_train = trainingSet[:, -1]
        x_test = testSet
        # y_test = testSet[:, -1] ## bu yok?

        # Create linear regression object
        regr = linear_model.LinearRegression()

        #print("x_train", x_train)
        #print("y_train", y_train)

        # Train the model using the training sets
        regr.fit(x_train, y_train)

        # Make predictions using the testing set
        #results = regr.predict(x_test)
        model = regr.coef_.tolist()
        #print(model)

        modelNew = [int(x * 1000000) for x in model]
        resultsTemp = np.dot(x_test, modelNew)
        results = [x//1000000 for x in resultsTemp]
        #print(results)
        
        resultsHashed = ""
        for elt in results:
            resultsHashed = resultsHashed + (hashlib.sha256(str(elt).encode('utf-8')).hexdigest()) + ","
            
            
        context = zmq.Context()
        HOST = str(IP+":"+str(int(portNumber) + 1))
        php = "tcp://"+ HOST
        s = context.socket(zmq.REQ)
        s.connect(php) # block until connected
        s.send_string( json.dumps(results)) # send message
        message = s.recv().decode()
        print("message", message)

        #print(results)
        
        pk = paillier.PaillierPublicKey(int(pk1n))

        modelStored = model
        encryptedModel = []
        hashedModel = ""
        for i in model:
            iEncrypted = pk.encrypt(i)
            hashedModel = hashedModel + (hashlib.sha256(str(i).encode('utf-8')).hexdigest()) + ","
            encryptedModel.append(iEncrypted)
        #print(encryptedModel)
        
        Json = {}
        Json['testOwner'] = fromAddress
        Json['hashedModel'] = hashedModel
        Json['hashedResults'] = resultsHashed
        
        return json.dumps(Json)
    
    else:
        Json = {}
        Json['testOwner'] = "dummy"        
        Json['hashedModel'] = ""
        Json['hashedResults'] = ""
        return json.dumps(Json)        

@app.route("/client", methods = ['POST'])
def client():
    global pk
    global sk
    global exponents
    print("process started")
    #path = (request.get_json())['path']
    #print(path)

    pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic public and private keys generated
    pk = pk1
    sk = sk1

    enc_with_one_pub_key = {}
    enc_with_one_pub_key['pk1'] = {'g': str(pk1.g),'n': str(pk1.n)}

    pathTest = "/home/ege/blockchain/server/smalltestx.csv"
    #pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltestx.csv"
    f = open(pathTest)
    test = []
    csv_reader = csv.reader(f, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        row = row[0].split(',')
        numrow =[]
        for num in row:
            numrow.append(float(num))
        test.append(numrow)
    f.close()

    pathResult = pathTest = "/home/ege/blockchain/server/smalltesty.csv"
    #pathResult = pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltesty.csv"
    f = open(pathResult)
    testResultsEncrpyted = []  ## encrypted not hashed test results
    testResultsEncrpytedJson = []
    csv_reader = csv.reader(f, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        encryptedRow = pk1.encrypt(float(row[0]))
        testResultsEncrpytedJson.append((str(encryptedRow.ciphertext()), encryptedRow.exponent))
        exponents.append(encryptedRow.exponent)
    f.close()
    #print("encrypted not hashed test results", testResultsEncrpytedJson)

    testResultJSON = [] ## encrypted and hashed test results
    for x in testResultsEncrpytedJson:
        h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
        hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
        testResultJSON.append((h, hExponent))

    #print("encrypted and hashed test results", testResultJSON)

    # first encrypt test with a symmetric key, then use paillier?

    testEncryptedJson = []
    for r in test:
        encrypted_r = []
        for p in r:
            encryptedP = (pk1.encrypt(p))
            encrypted_r.append((str(encryptedP.ciphertext()), encryptedP.exponent))
        testEncryptedJson.append(encrypted_r)

    testEncryptedHashedJSON = []
    for i in testEncryptedJson:
        testjson_i = []
        for x in i:
            h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
            hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
            testjson_i.append((h, hExponent))
        testEncryptedHashedJSON.append(testjson_i)

    context = zmq.Context()
    HOST = str(IP+":"+str(portNumber))
    php = "tcp://"+ HOST
    s = context.socket(zmq.REQ)
    s.connect(php) # block until connected
    s.send_string(json.dumps(testEncryptedJson)) # send message
    message = s.recv().decode()
    print("message", message)

    ##encrypted_test ## send to other guy

    enc_with_one_pub_key['hashedTest'] = testEncryptedHashedJSON
    enc_with_one_pub_key['hashedTestResults'] = testResultJSON

    #    """enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in encrypted_test]
    #        serialised = json.dumps(enc_with_one_pub_key)"""

    #enc_with_one_pub_key['pk2'] = pk2json['pk2']
    serialised = json.dumps(enc_with_one_pub_key)

    print("finito")
    return serialised #, pk2json

t1 = threading.Thread(target=receiveRequest, )
t2 = threading.Thread(target=peerAPI, )
t3 = threading.Thread(target=receiveRequestResult, )
t4 = threading.Thread(target=receiveModel, )

t1.start()
t2.start()
t3.start()
t4.start()

"""
/home/ege/blockchain/server/smalltesty.csv
/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server
"""
