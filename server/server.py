# coding=utf-8
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Hash import SHA
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from Crypto.PublicKey import RSA
from Crypto import Random
import base64
import csv
from phe import paillier
import json
import sys
import os
import threading
import hashlib
import pickle
import zmq
from decimal import *

app = Flask(__name__)
CORS(app)
portNumber = sys.argv[2]
portNumberSelf = sys.argv[1]
host = sys.argv[3]
port = sys.argv[4]
IP = "127.0.0.1"
encryptedTest_pickle = []
encryptedResults = []
results = []
encryptedModel = []
model = []
hashedCoef = []
pk = ""
sk = ""
exponents = []
processTraining = True
requestDataEventNumber = 0
sendResultEventNumber = 0
decideResultEventNumber = 0
sendModelEventNumber = 0

def peerAPI():
    #api.add_resource(peerConnect, '/peerConnect/')
    app.run(debug=False,host=str(host), port=int(port))

def receiveRequest():
    global encryptedTest_pickle
    context = zmq.Context()
    HOST = str(IP+":"+str(portNumberSelf))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedTest_pickle = pickle.loads((s.recv()))
    s.send(b'message received')
    
def receiveRequestResult():
    global encryptedResults
    context = zmq.Context()
    HOST = str(IP+":"+str(int(portNumberSelf)+1))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedResults = pickle.loads((s.recv()))
    s.send(b'message received')

def receiveModel():
    global encryptedModel
    context = zmq.Context()
    HOST = str(IP+":"+str(int(portNumberSelf)+2))
    php = "tcp://"+ HOST # how and where to connect
    print(php)
    s = context.socket(zmq.REP)
    s.bind(php)
    encryptedModel = pickle.loads((s.recv()))
    s.send(b'message received')

@app.route("/")
def helloWorld():
    return jsonify("fee")


@app.route("/getModel", methods = ['POST'])
def getModel():
    data = (request.get_json())['data']
    global sendModelEventNumber
    incomingNumber = int(data['sendModelEventNumber'])
    if incomingNumber > sendModelEventNumber:
        sendModelEventNumber += 1
        print("-------------------getModel start-----------")
        global hashedCoef
        global encryptedModel
        global pk
        global sk
        print("received the model")
        data = (request.get_json())['data']
        fromAddress = data['from']
        toAddress = data['to']

        print(pk)
        print(encryptedModel)
        model = [sk.decrypt(x) for x in encryptedModel['encryptedModel']]

        print("hashing to check the model")
        modelHash = ""
        for i in model:
            modelHash = modelHash + hashlib.sha256(str(i).encode('utf-8')).hexdigest() + ","
        print(modelHash == hashedCoef)

        pathTest = "/home/ege/blockchain/server/smalltestx.csv"
        #pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltestx.csv"
        f = open(pathTest)
        test = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            row = row[0].split(',')
            numrow =[]
            for num in row:
                numrow.append(float(num))
            test.append(numrow)
        f.close()

        computedResults = np.dot(test, model)
        print(computedResults)
        print(results)
    
        match = True
        for i in range(0, len(computedResults)):
            if round(computedResults[i], 8) != round(results[i], 8):
                match = False
        print(match)
        
        todump = {}
        todump['dataOwner'] = fromAddress
        if match:
            todump['decision'] = 1 # confirm
        else:
            todump['decision'] = 0 # reject

        print("-------------------getModel end-----------")
        return json.dumps(todump)

    else:
        todump = {}
        todump['dataOwner'] = "dummy"
        todump['decision'] = -1
        return json.dumps(todump)

@app.route("/sendModel", methods = ['POST'])
def sendModel():
    data = (request.get_json())['data']
    global decideResultEventNumber
    incomingNumber = int(data['decideResultEventNumber'])
    if incomingNumber > decideResultEventNumber:
        decideResultEventNumber += 1
        print("-------------------sendModel start-----------")
        global encryptedModel
        print("sending the model")
        data = (request.get_json())['data']
        fromAddress = data['from']
        toAddress = data['to']

        todumpToOtherUser = {}
        todumpToOtherUser['testOwner'] = fromAddress
        todumpToOtherUser['encryptedModel'] = encryptedModel

        todump = {}
        todump['testOwner'] = fromAddress
        todump['encryptedModel'] = [(str(x.ciphertext()), x.exponent) for x in encryptedModel]
        
        context = zmq.Context()
        HOST = str(IP+":"+str(int(portNumber) + 2))
        php = "tcp://"+ HOST
        s = context.socket(zmq.REQ)
        s.connect(php) # block until connected
        s.send( pickle.dumps(todumpToOtherUser)) # send message
        message = s.recv().decode()
        print("message", message)
        
        print("-------------------sendModel end-----------")

        return json.dumps(todump)

    else:
        todump = {}
        todump['testOwner'] = "dummy"
        todump['encryptedModel'] = []
        return json.dumps(todump)

@app.route("/getResults", methods = ['POST'])
def getResults():
    data = (request.get_json())['data']
    global sendResultEventNumber
    incomingNumber = int(data['sendResultEventNumber'])
    if incomingNumber > sendResultEventNumber:
        sendResultEventNumber += 1
        print("-------------------getResults start-----------")

        global encryptedResults
        global hashedCoef
        global results
        data = (request.get_json())['data']
        fromAddress = data['from']
        toAddress = data['to']
        coef = data['coef']
        results = data['results']   
        hashedCoef = coef

        print("hashing to check the results")
        encResultsHash = ""
        for i in encryptedResults:
            encResultsHash = encResultsHash + hashlib.sha256(str(i.ciphertext()).encode('utf-8')).hexdigest() + ","
        print(encResultsHash == results)
        
        print("encrypted results", encryptedResults)

        print("accuracy check started")

        plainResults = [sk.decrypt(x) for x in encryptedResults]
        
        results = plainResults

        realResults = []
        pathResult = pathTest = "/home/ege/blockchain/server/smalltesty.csv"
        #pathResult = pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltesty.csv"
        f = open(pathResult)
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            realResults.append(float(row[0]))
        f.close()

        print(realResults)
        print(plainResults)

        accuracy = r2_score(realResults, plainResults)
        print("r2 score: ", accuracy)

        
        threshold = -999999999
        todump = {}
        todump['dataOwner'] = fromAddress
        if accuracy > threshold:
            todump['decision'] = 1 # confirm
        else:
            todump['decision'] = 0 # reject

        print("-------------------getResults end-----------")

        return json.dumps(todump)

    else:
        todump = {}
        todump['dataOwner'] = "dummy"
        todump['decision'] = -1
        return json.dumps(todump)

@app.route("/requestTraining", methods = ['POST'])
def requestTraining():
    data = (request.get_json())['data']
    global requestDataEventNumber
    incomingNumber = int(data['requestDataEventNumber'])
    if incomingNumber > requestDataEventNumber:
        requestDataEventNumber += 1
        print("-------------------requestTraining start-----------")
        global encryptedTest_pickle
        global encryptedModel
        global modelStored
        print("training process started")
        data = (request.get_json())['data']
        fromAddress = data['from']
        toAddress = data['to']
        row = data['row']
        col = data['col']
        testCipher = data['testCipher']
        testExp = data['testExp']
        testResultCipher = data['testResultCipher']
        testResultExp = data['testResultExp']
        pk1g = data['pk1g']
        pk1n = data['pk1n']
        
        print("hashing to check test data")
        encryptedTestHash = ""
        encryptedTestHashExp = ""
        for i in encryptedTest_pickle:
            for j in i:
                encryptedTestHash = encryptedTestHash + hashlib.sha256(str(j.ciphertext()).encode('utf-8')).hexdigest() + ","
                encryptedTestHashExp = encryptedTestHashExp + hashlib.sha256(str(j.exponent).encode('utf-8')).hexdigest() + ","
        print(encryptedTestHash == testCipher)
        print(encryptedTestHashExp == testExp)

        # Load from your computer
        trainingPath = "/home/ege/blockchain/server/trainx.csv"
        #trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainx.csv"
        f = open(trainingPath)
        x_train = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            row = row[0].split(',')
            numrow =[]
            for num in row:
                numrow.append(float(num))
            x_train.append(numrow)
        f.close()

        trainingPath = "/home/ege/blockchain/server/trainy.csv"
        #trainingPath = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltrainy.csv"
        f = open(trainingPath)
        y_train = []
        csv_reader = csv.reader(f, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            y_train.append(float(row[0]))
        f.close()

        x_test = encryptedTest_pickle


        # Create linear regression object
        regr = linear_model.LinearRegression()
        regr.fit(x_train, y_train)
        model = regr.coef_.tolist()
        
        results = np.dot(x_test, model)

        print(results)
        
        resultsHashed = ""
        for elt in results:
            resultsHashed = resultsHashed + (hashlib.sha256(str(elt.ciphertext()).encode('utf-8')).hexdigest()) + ","

        print(resultsHashed)
        context = zmq.Context()
        HOST = str(IP+":"+str(int(portNumber) + 1))
        php = "tcp://"+ HOST
        s = context.socket(zmq.REQ)
        s.connect(php) # block until connected
        s.send( pickle.dumps(results)) # send message
        message = s.recv().decode()
        print("message", message)

        pk = paillier.PaillierPublicKey(int(pk1n))
        modelStored = model
        encryptedModel = []
        hashedModel = ""
        for i in model:
            iEncrypted = pk.encrypt(i)
            hashedModel = hashedModel + (hashlib.sha256(str(i).encode('utf-8')).hexdigest()) + ","
            encryptedModel.append(iEncrypted)

        Json = {}
        Json['testOwner'] = fromAddress
        Json['hashedModel'] = hashedModel
        Json['hashedResults'] = resultsHashed

        print("-------------------requestTraining end-----------")

        return json.dumps(Json)
        
    else:
        Json = {}
        Json['testOwner'] = "dummy"        
        Json['hashedModel'] = ""
        Json['hashedResults'] = ""
        return json.dumps(Json)    

@app.route("/client", methods = ['POST'])
def client():
    global pk
    global sk
    global exponents
    print("process started")

    pk1, sk1 = paillier.generate_paillier_keypair() # homomorphic public and private keys generated
    pk = pk1
    sk = sk1

    enc_with_one_pub_key = {}
    enc_with_one_pub_key['pk1'] = {'g': str(pk1.g),'n': str(pk1.n)}

    pathTest = "/home/ege/blockchain/server/smalltestx.csv"
    #pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltestx.csv"
    f = open(pathTest)
    test = []
    csv_reader = csv.reader(f, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        row = row[0].split(',')
        numrow =[]
        for num in row:
            numrow.append(float(num))
        test.append(numrow)
    f.close()

    pathResult = pathTest = "/home/ege/blockchain/server/smalltesty.csv"
    #pathResult = pathTest = "/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server/smalltesty.csv"
    f = open(pathResult)
    testResultsEncrpyted = []  ## encrypted not hashed test results
    testResultsEncrpytedJson = []
    csv_reader = csv.reader(f, delimiter=';')
    next(csv_reader)
    for row in csv_reader:
        encryptedRow = pk1.encrypt(float(row[0]))
        testResultsEncrpytedJson.append((str(encryptedRow.ciphertext()), encryptedRow.exponent))
        exponents.append(encryptedRow.exponent)
    f.close()

    testResultJSON = [] ## encrypted and hashed test results
    for x in testResultsEncrpytedJson:
        h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
        hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
        testResultJSON.append((h, hExponent))


    testEncryptedJson = []
    testEncryptedJson_pickle = []
    for r in test:
        encrypted_r = []
        encrypted_r_pickle = []
        for p in r:
            encryptedP = (pk1.encrypt(p))
            encrypted_r.append((str(encryptedP.ciphertext()), encryptedP.exponent))
            encrypted_r_pickle.append(encryptedP)
        testEncryptedJson.append(encrypted_r)
        testEncryptedJson_pickle.append(encrypted_r_pickle)

    testEncryptedHashedJSON = []
    for i in testEncryptedJson:
        testjson_i = []
        for x in i:
            h = hashlib.sha256(str(x[0]).encode('utf-8')).hexdigest()
            hExponent = hashlib.sha256(str(x[1]).encode('utf-8')).hexdigest()
            testjson_i.append((h, hExponent))
        testEncryptedHashedJSON.append(testjson_i)

    context = zmq.Context()
    HOST = str(IP+":"+str(int(portNumber) + 0))
    php = "tcp://"+ HOST
    s = context.socket(zmq.REQ)
    s.connect(php) # block until connected
    s.send(pickle.dumps(testEncryptedJson_pickle)) # send message
    message = s.recv().decode()
    print("message", message)

    enc_with_one_pub_key['hashedTest'] = testEncryptedHashedJSON
    enc_with_one_pub_key['hashedTestResults'] = testResultJSON

    serialised = json.dumps(enc_with_one_pub_key)

    print("finito")
    return serialised

t1 = threading.Thread(target=receiveRequest, )
t2 = threading.Thread(target=peerAPI, )
t3 = threading.Thread(target=receiveRequestResult, )
t4 = threading.Thread(target=receiveModel, )

t1.start()
t2.start()
t3.start()
t4.start()

"""
/home/ege/blockchain/server/smalltesty.csv
/Users/msahtopcuoglu/Desktop/blockchain/blockchain/server
"""
