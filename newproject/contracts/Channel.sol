pragma solidity ^0.5.0;
//pragma experimental ABIEncoderV2;

import "./token/ERC20.sol";

// //import "./MetaCoin.sol";

// contract MetaCoin {
//     function sendCoinAccepted(address sender, address receiver, uint amount) public;
// }

contract Channel {
    event sendTrainingRequestEvent(address indexed from, address indexed to);
    event acceptTrainingRequestEvent(address indexed from, address indexed to);
    // change variables in line with the standard naming
    event defineTSPEvent(address indexed from, uint index, uint[] matrix, uint n, uint[] path, uint len, uint basePrice, uint rate);
    event solveTSPEvent(address indexed from, address indexed to, uint[] path, uint len, uint indexfe);
    event defineGraphColEvent(address indexed from, uint index, uint[] instance, uint instanceSize,
    uint[] coloring, uint k_min, uint basePrice, uint rate);
    event solveGraphColEvent(address indexed from, address indexed to, uint[] coloring, uint k, uint indexfe);
    event defineIntPtnEvent(address indexed from, uint index, uint instance, uint[] partition, uint[] indices,
    uint numPtn, uint basePrice, uint rate);
    event solveIntPtnEvent(address indexed from, address indexed to, uint[] partition, uint[] indices, uint numPtn, uint indexfe);
    event defineDataEvent(address indexed from, uint index, string name, uint price, string description, string header);
    event requestDataEvent(address indexed from, address indexed to, uint row, uint col, string testCipher, string testExp,
        string testResultCipher, string testResultExp, string pk1g, string pk1n, uint requestDataEventNumber);
    event sendResultEvent(address indexed from, address indexed to, string coef, string results, uint sendResultEventNumber);
    event decideResultEvent(address indexed from, address indexed to, uint decision, uint decideResultEventNumber);
    event sendModelEvent(address indexed from, address indexed to, uint sendModelEventNumber);
    //event definePrimeFacEvent(address indexed from, uint index, uint instance, uint price);
    enum RelationshipType {NoRelation, Requested, Accepted, Rejected}

    struct TSP {
        uint size;
        uint[] instance;
        uint[] minPath;
        uint minLen;
        uint basePrice;
        uint rate;
    }

    struct GraphCol {
        uint size;
        uint[] instance;
        uint[] coloring;
        uint k_min;
        uint basePrice;
        uint rate;
    }

    struct IntPtn {
        uint instance;
        uint[] partition;
        uint[] indices;
        uint numPtn;
        uint basePrice;
        uint rate;
        uint pid;
        //mapping (string => bool) exists;
    }

    struct Data {
        string name;
        uint price;
        string description;
        string header;
    }

    struct Test {
        uint row;
        uint col;
        string testCipher;
        string testExp;
        string testResultCipher;
        string testResultExp;
        string pk1g;
        string pk1n;
        string pk2;
        address owner;
    }

    struct Model {
        string coef;
        string results;
        address owner;
    }

    /*struct PrimeFac {
        uint instance;
        uint[] factorization;
        uint price;
    }*/

    struct Person {
        bytes32 name;
        bool isMember;
        uint amount;
        uint number;
        TSP[] tspList;
        GraphCol[] gcList;
        IntPtn[] ipList;
        Data[] dataList;
        //Test[] testList;
        Model[] modelList;
        //PrimeFac[] pfList;
    }

    uint pid = 0;
    uint requestDataEventNumber = 0;
    uint sendResultEventNumber = 0;
    uint decideResultEventNumber = 0;
    uint sendModelEventNumber = 0;
    mapping (address => mapping (address => RelationshipType)) relationships;
    mapping (address => Person) public people;
    mapping (uint => mapping (string => bool)) exists;
    
    /* TSP functions */
    function defineTSP(uint[] memory matrix, uint instanceSize, uint[] memory path, uint basePrice, uint rate) public {
        TSP memory init;
        init.instance = matrix;
        init.size = instanceSize;
        init.minPath = path;
        if (path.length == 0) {
            init.minLen = 1 << 255;
        }
        else {
            require(path[0] == path[path.length-1], "Initial path is not a cycle.");
            uint length = 0;
            for(uint i = 0; i < path.length - 1 ; i++) {
                require(matrix[path[i] * instanceSize + path[i+1]] > 0, "Initial path contains an invalid edge.");
                length += matrix[path[i] * instanceSize + path[i+1]];
            }
            init.minLen = length;
        }
        init.basePrice = basePrice;
        init.rate = rate;
        
        people[msg.sender].tspList.push(init);

        emit defineTSPEvent(msg.sender, people[msg.sender].tspList.length - 1, matrix, instanceSize, path, init.minLen, basePrice, rate);
    }

    function sendPath(address problemOwner, uint index, uint instanceSize, uint[] memory path, address contractAddress, uint indexfe) public {
        ERC20 erc20 = ERC20(contractAddress);
        TSP memory temp = people[problemOwner].tspList[index];

        uint candidateLen = checkPath(problemOwner, index, instanceSize, path);
        require(candidateLen < temp.minLen, "No improvement.");
        
        uint prize;
        if (temp.minPath.length > 0) {
            prize = temp.basePrice * (temp.rate ** (temp.minLen - candidateLen));
        }
        else {
            prize = temp.basePrice * temp.rate;
        }

        people[problemOwner].tspList[index].minPath = path;
        people[problemOwner].tspList[index].minLen = candidateLen;

        erc20._transfer(problemOwner, msg.sender, prize);
    
        emit solveTSPEvent(msg.sender, problemOwner, path, candidateLen, indexfe);
    }

    function checkPath(address problemOwner, uint index, uint instanceSize, uint[] memory path) public returns (uint) {
        require(path[0] == path[path.length-1], "Path is not a cycle.");
        
        uint[] memory temp = people[problemOwner].tspList[index].instance;
        uint score = 0;

        for(uint i = 0; i < path.length - 1 ; i++) {
            require(temp[path[i] * instanceSize + path[i+1]] > 0, "Path contains an invalid edge.");
            score += temp[path[i] * instanceSize + path[i+1]];
        }

        return score;
    }

    /* GraphCol functions */
    function defineGraphCol(uint[] memory instance, uint instanceSize, uint[] memory coloring, uint basePrice, uint rate) public {
        GraphCol memory init;
        init.instance = instance;   // assumed no self-loops
        init.size = instanceSize;
        init.coloring = coloring;
        if (coloring.length == 0) {     // given empty array as initial coloring
            init.k_min = instanceSize;
        }
        else {
            require(coloring.length == instanceSize, "Initial coloring is not complete.");
            uint k = 0;
            for (uint i = 0; i < instanceSize; i++) {   // color labels: 0 to n-1  ==>  find the largest label
                if (coloring[i] > k) {
                    k = coloring[i];
                }
            }
            
            init.k_min = k + 1;

            for (uint i = 0; i < instanceSize; i++) {   // get the color of node i
                uint color = coloring[i];
                
                for (uint j = 0; j < instanceSize; j++) { // check the coloring of neighbors of i
                    if (instance[i * instanceSize + j] == 1) {
                        require(color != coloring[j], "Initial coloring is invalid.");
                    }
                }
            }
        }

        init.basePrice = basePrice;
        init.rate = rate;
        
        people[msg.sender].gcList.push(init);

        emit defineGraphColEvent(msg.sender, people[msg.sender].gcList.length - 1, instance, instanceSize, coloring, init.k_min, basePrice, rate);
    }

    function sendColoring(address problemOwner, uint index, uint instanceSize, uint[] memory coloring, address contractAddress, uint indexfe) public {
        ERC20 erc20 = ERC20(contractAddress);
        GraphCol memory temp = people[problemOwner].gcList[index];

        uint k = checkColoring(problemOwner, index, instanceSize, coloring);
        require(k < temp.k_min, "No improvement.");

        uint prize = temp.basePrice * (temp.rate ** (temp.k_min - k));

        people[problemOwner].gcList[index].coloring = coloring;
        people[problemOwner].gcList[index].k_min = k;

        erc20._transfer(problemOwner, msg.sender, prize);
    
        emit solveGraphColEvent(msg.sender, problemOwner, coloring, k, indexfe);
    }

    function checkColoring(address problemOwner, uint index, uint instanceSize, uint[] memory coloring) public returns (uint) {
        require(coloring.length == instanceSize, "Not a complete coloring.");
        uint[] memory temp = people[problemOwner].gcList[index].instance;
        uint k = 0;
        for (uint i = 0; i < instanceSize; i++) {   // get the color of node i
            uint color = coloring[i];
            
            for (uint j = 0; j < instanceSize; j++) { // check the coloring of neighbors of i
                if (temp[i * instanceSize + j] == 1) {
                    require(color != coloring[j], "Invalid coloring.");
                }
            }

            if (color > k) {
                k = color;
            }
        }

        return k + 1;
    }

    /* IntPtn functions */
    function defineIntPtn(uint instance, uint[] memory partition, uint[] memory indices, uint basePrice, uint rate) public {
        IntPtn memory init;
        init.instance = instance;
        init.partition = partition;
        init.indices = indices;
        init.numPtn = indices.length - 1;
        init.pid = pid;
        pid++;
        
        for (uint i = 0; i < indices.length - 1; i++) {   // sort given the partition in decrasing order
            for (uint pass = 0; pass < indices[i+1] - indices[i]; pass++) {
                for (uint j = indices[i]; j < indices[i+1] - 1; j++) {
                    if (partition[j] < partition[j+1]) {
                        uint temp = partition[j+1];
                        partition[j+1] = partition[j];
                        partition[j] = temp;
                    }
                }
            }
        }
        
        for(uint i = 0; i < indices.length - 1; i++) {
            uint sum = 0;
            for (uint j = indices[i]; j < indices[i+1]; j++) {
                sum += partition[j];
            }
            require(sum == instance, "Initial partition are invalid.");
            
            string memory ptn = "";
            for (uint j = indices[i]; j < indices[i+1]; j++) {
                ptn = string(abi.encodePacked(ptn, "+", uint2str(partition[j]), "", ""));
            }
            require(exists[init.pid][ptn] == false, "Initial partitions contain duplicates.");
            exists[init.pid][ptn] = true;
        }
        
        init.basePrice = basePrice;
        init.rate = rate;
        
        people[msg.sender].ipList.push(init);
    
        emit defineIntPtnEvent(msg.sender, people[msg.sender].ipList.length - 1, instance, partition, indices, init.numPtn, basePrice, rate);
    }
    
    function sendPartition(address problemOwner, uint index, uint[] memory partition, uint[] memory indices, address contractAddress, uint indexfe) public {
        ERC20 erc20 = ERC20(contractAddress);
        IntPtn memory temp = people[problemOwner].ipList[index];

        uint newIndicesLen = checkPartition(problemOwner, index, partition, indices);
        //require(isPtnValid, "No improvement.");

        for (uint i = 0; i < newIndicesLen - 1; i++) {
            for (uint j = indices[i]; j < indices[i+1]; j++) {
                people[problemOwner].ipList[index].partition.push(partition[j]);
            }
            people[problemOwner].ipList[index].indices.push(people[problemOwner].ipList[index].partition.length);
            /*uint indexNew = indices[i+1];
            uint indexLast = people[problemOwner].ipList[index].indices[people[problemOwner].ipList[index].indices.length - 1];
            people[problemOwner].ipList[index].indices.push(indexLast + indexNew);*/
            
        }
        people[problemOwner].ipList[index].numPtn += newIndicesLen - 1;
        
        uint prize;
        if (temp.partition.length > 0) {
            prize = temp.basePrice * (temp.rate ** (newIndicesLen - 1));
        }
        else {
            prize = temp.basePrice * temp.rate;
        }

        erc20._transfer(problemOwner, msg.sender, prize);
        
        temp = people[problemOwner].ipList[index];
    
        emit solveIntPtnEvent(msg.sender, problemOwner, temp.partition, temp.indices, temp.numPtn, indexfe);
    }
    
    function checkPartition(address problemOwner, uint index, uint[] memory partition, uint[] memory indices) public returns (uint) {
        for (uint i = 0; i < indices.length - 1; i++) {   // for each given partition
            for (uint pass = 0; pass < indices[i+1] - indices[i]; pass++) { // sort in decreasing order
                for (uint j = indices[i]; j < indices[i+1] - 1; j++) {
                    if (partition[j] < partition[j+1]) {
                        uint temp = partition[j+1];
                        partition[j+1] = partition[j];
                        partition[j] = temp;
                    }
                }
            }
            
            uint sum = 0;
            for (uint j = indices[i]; j < indices[i+1]; j++) {
                sum += partition[j];
            }
            require(sum == people[problemOwner].ipList[index].instance, "Invalid partition.");
            
            
            string memory ptn = "";
            for (uint j = indices[i]; j < indices[i+1]; j++) {
                ptn = string(abi.encodePacked(ptn, "+", uint2str(partition[j]), "", ""));
            }
            require(exists[people[problemOwner].ipList[index].pid][ptn] == false, "Given partition is already known or contains duplicates.");
            exists[people[problemOwner].ipList[index].pid][ptn] = true;
        }

        return indices.length;
    }

    /* ML functions */
    function defineData(string memory name, uint price, string memory description, string memory header) public {
        Data memory init;
        init.name = name;
        init.price = price;
        init.description = description;
        init.header = header;

        people[msg.sender].dataList.push(init);

        emit defineDataEvent(msg.sender, people[msg.sender].dataList.length - 1, name, price, description, header);
    }

    function requestData(address dataOwner, uint row, uint col, string memory testCipher, string memory testExp,
    string memory testResultCipher, string memory testResultExp, string memory pk1g, string memory pk1n) public {
        Test memory temp;
        temp.row = row;
        temp.col = col;
        temp.testCipher = testCipher;
        temp.testExp = testExp;
        temp.testResultCipher = testResultCipher;
        temp.testResultExp = testResultExp;
        temp.pk1g = pk1g;
        temp.pk1n = pk1n;
        temp.owner = msg.sender;

        //people[msg.sender].testList.push(temp);
        requestDataEventNumber++;
        emit requestDataEvent(msg.sender, dataOwner, row, col, testCipher, testExp, testResultCipher, testResultExp, pk1g, pk1n, requestDataEventNumber);
    }

    function sendResult(address testOwner, string memory coef, string memory results) public {
        Model memory temp;
        temp.coef = coef;
        temp.results = results;
        temp.owner = msg.sender;

        people[msg.sender].modelList.push(temp);

        sendResultEventNumber++;
        emit sendResultEvent(msg.sender, testOwner, temp.coef, temp.results, sendResultEventNumber);
    }

    function decideResult(address dataOwner, uint  decision) public {
        if (decision == 1) {
            decideResultEventNumber++;
            emit decideResultEvent(msg.sender, dataOwner, decision, decideResultEventNumber);
        }
        else {
            // TODO: Test owner did not confirm the results.
            uint doSomething = 0;
            doSomething++;
        }
    }

    function sendModel(address testOwner) public {
        sendModelEventNumber++;
        emit sendModelEvent(msg.sender, testOwner, sendModelEventNumber);
    }

    function decideModel(address dataOwner, uint decision, uint amount, address contractAddress) public {
        if (decision == 1) {
            ERC20 erc20 = ERC20(contractAddress);
            erc20._transfer(msg.sender, dataOwner, amount);
        }
        else {
            // TODO: Test owner did not confirm the model.
            uint doSomething = 0;
            doSomething++;
        }
    }

    /*function getPartition(address addr, uint index) public returns(uint[][] memory){
        return people[addr].ipList[index].partition;
    }*/

    /* PrimeFac functions*/
    /*function definePrimeFac(uint instance, uint price) public {
        PrimeFac memory init;
        init.instance = instance;
        init.price = price;

        people[msg.sender].pfList.push(init);

        emit definePrimeFacEvent(msg.sender, people[msg.sender].pfList.length - 1, instance, price);
    }
    
    function solvePrimeFac(address problemOwner, uint index, uint[] memory factorization, address contractAddress) public {
        int ans = 1;
        for (int i = 0; i < factorization.length; i++) {
            //require(factorization[i] is prime, "Composite number in the factorization.")
            ans = ans * factorization[i];
        }
        
        require(ans == people[problemOwner].pfList[index].instance, "Prime factorization is not correct.")
    }*/


    /* Other functions */
    function sendTrainingRequest(address addr, uint amount) public {
        //require(relationships[msg.sender][addr] == RelationshipType.NoRelation, "there is relationship between client and provider");
        //require(relationships[addr][msg.sender] == RelationshipType.NoRelation, "there is relationship between provider and client");

        people[msg.sender].number = random();
        people[msg.sender].amount = amount;

        relationships[msg.sender][addr] = RelationshipType.Requested;
        emit sendTrainingRequestEvent(msg.sender, addr);
    }

    function acceptTrainingRequest(address addr, address contractAddress) public {
        //require(relationships[msg.sender][addr] == RelationshipType.NoRelation, "there is relationship between client and provider");
        //require(relationships[addr][msg.sender] == RelationshipType.Requested, "there is relationship between provider and client");

        require(people[addr].number == random(), "numbers are not equal");

        ERC20 erc20 = ERC20(contractAddress);

        relationships[msg.sender][addr] = RelationshipType.Accepted;
        relationships[addr][msg.sender] = RelationshipType.Accepted;

        erc20._transfer(addr, msg.sender, people[addr].amount);

        emit acceptTrainingRequestEvent(msg.sender, addr);
    }

    function random() private view returns (uint8) {
        return uint8(uint256(keccak256(abi.encodePacked(now))) % 2);
    }

    modifier onlyMember() {
        require(people[msg.sender].isMember == true, "client is not member");
        _;
    }

// TODO: implement join
  /*function join(bytes32 publicKeyLeft, bytes32 publicKeyRight) public {
        require(people[msg.sender].isMember == false, "you are already member");
        
        Person memory newMember = Person("", "", 0, true);
        people[msg.sender] = newMember;
    }*/
    
    function uint2str(uint _i) internal pure returns (string memory _uintAsString) {
        if (_i == 0) {
            return "0";
        }
        uint j = _i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len - 1;
        while (_i != 0) {
            bstr[k--] = byte(uint8(48 + _i % 10));
            _i /= 10;
        }
        return string(bstr);
    }
}