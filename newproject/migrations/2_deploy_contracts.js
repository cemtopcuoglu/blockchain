var ERC20 = artifacts.require("./token/ERC20.sol");
var safeMath = artifacts.require("./token/SafeMath.sol");
var Channel = artifacts.require("./Channel.sol");

module.exports = function (deployer) {
  deployer.deploy(safeMath);
  deployer.deploy(ERC20);
  deployer.deploy(Channel);
};