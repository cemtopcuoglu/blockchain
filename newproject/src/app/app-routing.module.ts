import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MetaSenderComponent }   from './meta/meta-sender/meta-sender.component';

const routes: Routes = [
  { path: '', redirectTo: '/metasender', pathMatch: 'full' },
  { path: 'metasender', component: MetaSenderComponent },
  //{ path: 'create', component: TSPCreateComponent },
  //{ path: 'instance', component: TSPInstanceComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}