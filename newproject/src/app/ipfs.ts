
import { InjectionToken } from '@angular/core';
import ipfs from 'ipfs';

export const IPFS = new InjectionToken(
    'The IPFS instance',
    {
        providedIn: 'root',
        factory: () => new ipfs()
    },
);