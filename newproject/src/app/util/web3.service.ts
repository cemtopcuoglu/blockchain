import { Injectable } from '@angular/core';
import contract from 'truffle-contract';
import { Subject, ReplaySubject } from 'rxjs';
declare let require: any;
const Web3 = require('web3');


declare let window: any;

@Injectable()
export class Web3Service {
  public web3: any;
  public web3_2: any;
  private accounts: string[];
  private accountsGanache: string[];
  public ready = false;

  public accountsObservable = new ReplaySubject<string[]>();
  public accountsObservableGanache = new ReplaySubject<string[]>();

  constructor() {
    window.addEventListener('load', (event) => {
      this.bootstrapWeb3();
    });
  }

  public bootstrapWeb3() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');

      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
    }

    this.web3_2 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
    setInterval(() => this.refreshAccounts(), 100);
    this.listAccounts();  // TODO: we may implement refreshing
  }

  public async artifactsToContract(artifacts) {
    if (!this.web3) {
      const delay = new Promise(resolve => setTimeout(resolve, 100));
      await delay;
      return await this.artifactsToContract(artifacts);
    }

    const contractAbstraction = contract(artifacts);
    contractAbstraction.setProvider(this.web3.currentProvider);
    return contractAbstraction;

  }

  private refreshAccounts() {
    this.web3.eth.getAccounts((err, accs) => {
      console.log('Refreshing accounts');

      if (err != null) {
        console.warn('There was an error fetching your accounts.');
        return;
      }

      // Get the initial account balance so it can be displayed.
      if (accs.length === 0) {
        console.warn('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.');
        return;
      }

      if (!this.accounts || this.accounts.length !== accs.length || this.accounts[0] !== accs[0]) {
        console.log('Observed new accounts');

        this.accountsObservable.next(accs);
        this.accounts = accs;
      }

      this.ready = true;
    });
  }

  private listAccounts() {
    this.web3_2.eth.getAccounts((err, accounts) => {
      console.log('listing accounts');

      if (err != null) {
        console.warn('There was an error fetching your accounts.');
        return;
      }

      // Get the initial account balance so it can be displayed.
      if (accounts.length === 0) {
        console.warn('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.');
        return;
      }

      if (!this.accounts || this.accounts.length !== accounts.length || this.accounts[0] !== accounts[0]) {
        console.log('Observed new accounts');

        this.accountsObservableGanache.next(accounts);
        this.accountsGanache = accounts;
      }
    })
  }

  public getAccount() {
    return this.web3.eth.defaultAccount;
  }

  public asciiToString(ascii) {
    console.log(ascii);
    return this.web3.utils.toAscii(ascii);
  }

  public getBlockNumber() {
    this.web3.eth.getBlockNumber().then((blockNumber) => {
      return blockNumber;
    });
  }

  public toBN(num) {
    return this.web3.utils.toBN(num);
  }
}
