import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

declare let require: any;
const channel_artifacts = require('../../../../build/contracts/Channel.json');
const ERC20_artifacts = require('../../../../build/contracts/ERC20.json');

@Component({
  selector: 'app-meta-sender',
  templateUrl: './meta-sender.component.html',
  styleUrls: ['./meta-sender.component.css']
})
export class MetaSenderComponent implements OnInit {
  accounts: string[];
  chosenAccounts: string[];
  Channel: any;
  ERC20: any;

  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    proposals: []
  };

  tsp = {
    size: null,
    basePrice: null,
    rate: null,
    problemMatrixText: '',
    path: [],
    pathText: '',
    problemMatrix: []
  }

  gc = {
    size: null,
    basePrice: null,
    rate: null,
    problemMatrixText: '',
    coloring: [],
    coloringText: '',
    problemMatrix: []
  }

  ip = {
    basePrice: null,
    rate: null,
    problemIntText: '',
    partition: null,
    partitionText: '',
    problemInt: null,
    numPtn: null,
    indices: null
  }

  status = '';

  // TODO: check the balance

  ////// New:
  problemTSPList: any[] = [];
  problemGCList: any[] = [];
  problemIPList: any[] = [];
  dataList: any[] = [];
  //////

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar,
    private route: ActivatedRoute, private router: Router, private httpClient: HttpClient) {
    console.log('Constructor: ' + web3Service);
    this.web3Service.artifactsToContract(ERC20_artifacts)
      .then((ERC20Abstraction) => {
        this.ERC20 = ERC20Abstraction;
      });
  }

  ngOnInit(): void {
    console.log('OnInit: ' + this.web3Service);
    console.log(this);

    this.httpClient.get('http://127.0.0.1:5000/client').subscribe((response: any) => {
      //console.log(data);
      //let a = data as JSON;
      console.log(response);
    })

    /*for (let j = 0; j < 400; j++) {
      this.problemMatrix[j] = 0;
    }

    
    let prob = {
      owner: 'Alice',
      size: 2,
      basePrice: 2,
      rate: 2,
      bestSolution: '[1 2 1]'
    };
    let prob2 = {
      owner: 'Bob',
      size: 3,
      basePrice: 1,
      rate: 3,
      bestSolution: '[2 3 2]'
    }*/

    //this.problemList.push(prob);
    //this.problemList.push(prob2);

    /*for(let i = 0; i < 20 ; i++) {
      this.problemMatrix[i] = [];
      for(let j = 0; j < 20 ; j++) {
        this.problemMatrix[i][j] = 0;
      }
    }*/

    this.watchAccount();

    this.web3Service.artifactsToContract(channel_artifacts) // TODO: clear the console outputs 
      .then((ChannelAbstraction) => {
        this.Channel = ChannelAbstraction;
        this.Channel.deployed().then(deployed => {
          console.log(deployed);
          console.log('heyyy');
          this.listPeople();
          deployed.sendTrainingRequestEvent({ filter: { from: this.model.account } }, (err, ev) => {
            console.log('sendTrainingRequestEvent event came in, select accept or reject');
            this.model.proposals.push("here", ev);
            //  TODO: this.refreshBalance();
          });
          deployed.acceptTrainingRequestEvent({ filter: { to: this.model.account } }, (err, ev) => {
            console.log('acceptTrainingRequestEvent event came in, select accept or reject');
          });

          deployed.defineTSPEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemTSPList.push(ev.returnValues);
            console.log("TSP problem", ev.returnValues);
          });
          deployed.solveTSPEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemTSPList[ev.returnValues.indexfe].path = ev.returnValues.path;
            this.problemTSPList[ev.returnValues.indexfe].len = ev.returnValues.len;
          });

          deployed.defineGraphColEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemGCList.push(ev.returnValues);
            console.log("GC problem", ev.returnValues);
          });
          deployed.solveGraphColEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemGCList[ev.returnValues.indexfe].coloring = ev.returnValues.coloring;
            this.problemGCList[ev.returnValues.indexfe].k_min = ev.returnValues.k;
          });

          deployed.defineIntPtnEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemIPList.push(ev.returnValues);
            console.log("IP problem", ev.returnValues);
          });
          deployed.solveIntPtnEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemIPList[ev.returnValues.indexfe].partition = ev.returnValues.partition;
            this.problemIPList[ev.returnValues.indexfe].indices = ev.returnValues.indices;
            this.problemIPList[ev.returnValues.indexfe].numPtn = ev.returnValues.numPtn;
          });

          deployed.defineDataEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.dataList.push(ev.returnValues);
            console.log("Data", ev.returnValues);
          });

        });

      });
  }

  watchAccount() {
    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.accounts = accounts;
        this.model.account = accounts[0];

      });
    }
    catch (e) {
      console.log(e);
    }
  }

  listAccounts() {
    console.log('list');
    this.web3Service.accountsObservableGanache.subscribe((accounts) => {
      console.log('accounts');
      console.log(accounts);
      this.chosenAccounts = accounts;
      this.model.receiver = accounts[0];

    })


  }

  public getName(name) {
    console.log(this.web3Service.asciiToString(name));
  }


  async listPeople() {
    try {
      let person;
      const deployedChannel = await this.Channel.deployed();
      deployedChannel.people(this.model.account).then((person) => {
        console.log(person);
        this.getName(person.name);
      });
    }
    catch (e) {
      console.log(e);
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  async propose() {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to send transaction');
      return;
    }

    console.log('proposed');

    const amount = this.model.amount;
    const receiver = this.model.receiver;

    try {
      console.log('burdayım');
      const deployedChannel = await this.Channel.deployed();

      const transaction = await deployedChannel.sendTrainingRequest.sendTransaction(receiver, amount, { from: this.model.account });

      console.log(transaction);
      console.log('1');
      if (!transaction) {
        console.log('ordayım');
        this.setStatus('Transaction failed!');
      } else {
        console.log('uzaktayım');
        this.setStatus('Transaction complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }

  async accept() {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to send transaction');
      return;
    }

    console.log('proposed');

    const amount = this.model.amount;
    const receiver = this.model.receiver;

    try {
      console.log('burdayım');
      const deployedChannel = await this.Channel.deployed();
      const deployedERC20 = await this.ERC20.deployed();

      const transaction = await deployedChannel.acceptTrainingRequest.sendTransaction(receiver, deployedERC20.address, { from: this.model.account });

      console.log(transaction);
      console.log('1');
      if (!transaction) {
        console.log('ordayım');
        this.setStatus('Transaction failed!');
      } else {
        console.log('uzaktayım');
        this.setStatus('Transaction complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }

  async solveTSP(ev: any, owner: any) {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to solve problem');
      return;
    }

    console.log('Sending answer to problem');

    console.log(this.tsp.path);
    console.log(owner);

    try {
      const deployedChannel = await this.Channel.deployed();
      const deployedERC20 = await this.ERC20.deployed();
      console.log('problem matrix', this.tsp.problemMatrix);
      console.log(this.model.account)
      console.log(deployedERC20.address);
      const sendPath = await deployedChannel.sendPath.sendTransaction(this.tsp.path,
        owner, this.tsp.size, 0, deployedERC20.address, { from: this.model.account }); // 0 seems to work but why?


      console.log(sendPath);
      if (!sendPath) {
        this.setStatus('Solve TSP failed!');
      } else {
        this.setStatus('Solve TSP complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error solving the problem; see log.');
    }
  }

  async solveGC(ev: any, owner: any) { // TODO
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to solve problem');
      return;
    }

    console.log('Sending answer to problem');

    console.log(this.gc.coloring);
    console.log(owner);

    try {
      const deployedChannel = await this.Channel.deployed();
      const deployedERC20 = await this.ERC20.deployed();
      console.log('problem matrix', this.tsp.problemMatrix);
      console.log(this.model.account);
      console.log(deployedERC20.address);
      const sendPath = await deployedChannel.sendPath.sendTransaction(this.tsp.path,
        owner, this.tsp.size, 0, deployedERC20.address, { from: this.model.account });


      console.log(sendPath);
      if (!sendPath) {
        this.setStatus('Solve GC failed!');
      } else {
        this.setStatus('Solve GC complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error solving the problem; see log.');
    }
  }

  setAmount(e) {
    console.log('Setting amount: ' + e.target.value);
    this.model.amount = e.target.value;
  }

  setReceiver(e) {
    console.log('Setting receiver: ' + e.target.value);
    this.model.receiver = e.target.value;
  }

  ngOnDestroy() {
    console.log("page destroyed");
    if (this.web3Service.accountsObservable.closed)
      this.web3Service.accountsObservable.unsubscribe();
    if (this.web3Service.accountsObservableGanache.closed)
      this.web3Service.accountsObservableGanache.unsubscribe();
  }
}
