import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MetaSenderComponent} from './meta-sender/meta-sender.component';
import {CreateComponent} from './create/create.component';
import {TSPInstanceComponent} from './tsp-instance/tsp-instance.component';
import {UserInfoComponent} from './user-info/user-info.component';
import {ProblemDetailComponent} from './problem-detail/problem-detail.component';
import {UtilModule} from '../util/util.module';
import {RouterModule} from '@angular/router';
import {MetaRoutingModule} from './meta-routing.module'
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule, MatSnackBarModule, MatListModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatSnackBarModule,
    MatListModule,
    RouterModule,
    UtilModule,
    FormsModule,
    MetaRoutingModule
    ],
  declarations: [MetaSenderComponent, CreateComponent, TSPInstanceComponent, UserInfoComponent, ProblemDetailComponent],
  exports: [MetaSenderComponent, CreateComponent, TSPInstanceComponent, UserInfoComponent, ProblemDetailComponent]
})
export class MetaModule {
}
