import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

declare let require: any;
const metacoin_artifacts = require('../../../../build/contracts/MetaCoin.json');
const channel_artifacts = require('../../../../build/contracts/Channel.json');

@Component({
  selector: 'app-meta-sender',
  templateUrl: './meta-sender.component.html',
  styleUrls: ['./meta-sender.component.css']
})
export class MetaSenderComponent implements OnInit {
  accounts: string[];
  chosenAccounts: string[];
  MetaCoin: any;
  Channel: any;
  Size: any;
  problem: any;
  problemMatrix: number[] = [];
  problems: any[] = [];

  path: any[] = [0, 2, 0]
  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    proposals: []
  };

  status = '';

  // TODO: check the balance

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar,
    private route: ActivatedRoute, private router: Router) {
    console.log('Constructor: ' + web3Service);
    this.web3Service.artifactsToContract(metacoin_artifacts)
      .then((MetaCoinAbstraction) => {
        this.MetaCoin = MetaCoinAbstraction;
      });
  }

  ngOnInit(): void {
    console.log('OnInit: ' + this.web3Service);
    console.log(this);

    for (let j = 0; j < 400; j++) {
      this.problemMatrix[j] = 0;
    }

    /*for(let i = 0; i < 20 ; i++) {
      this.problemMatrix[i] = [];
      for(let j = 0; j < 20 ; j++) {
        this.problemMatrix[i][j] = 0;
      }
    }*/

    this.watchAccount();
    this.listAccounts();
    this.web3Service.artifactsToContract(metacoin_artifacts)
      .then((MetaCoinAbstraction) => {
        this.MetaCoin = MetaCoinAbstraction;
        this.MetaCoin.deployed().then(deployed => {
          console.log(deployed);
          deployed.Transfer({}, (err, ev) => {
            console.log('Transfer event came in, refreshing balance');
            this.refreshBalance();
          });
        });

      });

    this.web3Service.artifactsToContract(channel_artifacts)
      .then((ChannelAbstraction) => {
        this.Channel = ChannelAbstraction;
        this.Channel.deployed().then(deployed => {
          console.log(deployed);
          console.log('heyyy');
          this.listPeople();
          deployed.sendTrainingRequestEvent({ filter: { from: this.model.account } }, (err, ev) => {
            console.log('sendTrainingRequestEvent event came in, select accept or reject');
            this.model.proposals.push("here", ev);
            //  TODO: this.refreshBalance();
          });
          deployed.acceptTrainingRequestEvent({ filter: { to: this.model.account } }, (err, ev) => {
            console.log('acceptTrainingRequestEvent event came in, select accept or reject');
          });
          deployed.defineTSPEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problems.push(ev.returnValues);
            console.log(ev);
          });
        });

      });
  }

  watchAccount() {
    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.accounts = accounts;
        this.model.account = accounts[0];
        this.refreshBalance();

      });
    }
    catch (e) {
      console.log(e);
    }
  }

  listAccounts() {
    console.log('list');
    this.web3Service.accountsObservableGanache.subscribe((accounts) => {
      console.log('accounts');
      console.log(accounts);
      this.chosenAccounts = accounts;
      this.model.receiver = accounts[0];
      this.refreshBalance();

    })


  }

  public getName(name) {
    console.log(this.web3Service.asciiToString(name));
  }


  async listPeople() {
    try {
      let person;
      const deployedChannel = await this.Channel.deployed();
      deployedChannel.people(this.model.account).then((person) => {
        console.log(person);
        this.getName(person.name);
      });
    }
    catch (e) {
      console.log(e);
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  async sendCoin() {
    if (!this.MetaCoin) {
      this.setStatus('Metacoin is not loaded, unable to send transaction');
      return;
    }

    const amount = this.model.amount;
    const receiver = this.model.receiver;

    console.log('Sending coins ' + amount + ' to ' + receiver);

    this.setStatus('Initiating transaction... (please wait)');
    try {
      const deployedMetaCoin = await this.MetaCoin.deployed();

      const transaction = await deployedMetaCoin.sendCoin.sendTransaction(receiver, amount, { from: this.model.account });

      if (!transaction) {
        this.setStatus('Transaction failed!');
      } else {
        this.setStatus('Transaction complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }

  async propose() {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to send transaction');
      return;
    }

    console.log('proposed');

    const amount = this.model.amount;
    const receiver = this.model.receiver;

    try {
      console.log('burdayım');
      const deployedChannel = await this.Channel.deployed();

      const transaction = await deployedChannel.sendTrainingRequest.sendTransaction(receiver, amount, { from: this.model.account });

      console.log(transaction);
      console.log('1');
      if (!transaction) {
        console.log('ordayım');
        this.setStatus('Transaction failed!');
      } else {
        console.log('uzaktayım');
        this.setStatus('Transaction complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }

  async accept() {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to send transaction');
      return;
    }

    console.log('proposed');

    const amount = this.model.amount;
    const receiver = this.model.receiver;

    try {
      console.log('burdayım');
      const deployedChannel = await this.Channel.deployed();
      const deployedMetaCoin = await this.MetaCoin.deployed();

      const transaction = await deployedChannel.acceptTrainingRequest.sendTransaction(receiver, deployedMetaCoin.address, { from: this.model.account });

      console.log(transaction);
      console.log('1');
      if (!transaction) {
        console.log('ordayım');
        this.setStatus('Transaction failed!');
      } else {
        console.log('uzaktayım');
        this.setStatus('Transaction complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }

  public refreshBalance() {
    console.log('Refreshing balance');
    try {
      this.MetaCoin.deployed().then(deployedMetaCoin => {
        deployedMetaCoin.getBalance.call(this.model.account).then(metaCoinBalance => {
          console.log('Found balance: ' + metaCoinBalance);
          this.model.balance = metaCoinBalance;
        });
      });
    } catch (e) {
      console.log(e);
      this.setStatus('Error getting balance; see log.');
    }
  }

  async solveProblem(ev: any, owner: any) {
    if (!this.Channel) {
      this.setStatus('Channel is not loaded, unable to solve problem');
      return;
    }

    console.log('sending answer to problem');

    console.log(this.path);
    console.log(owner);

    try {
      const deployedChannel = await this.Channel.deployed();
      const deployedMetaCoin = await this.MetaCoin.deployed();
      console.log('problem matrix', this.problemMatrix);
      console.log(this.model.account)
      console.log(deployedMetaCoin.address);
      const sendPath = await deployedChannel.sendPath.sendTransaction(this.path,
        owner, this.Size, 0, deployedMetaCoin.address, { from: this.model.account });


      console.log(sendPath);
      if (!sendPath) {
        this.setStatus('define problem failed!');
      } else {
        this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
      this.setStatus('Error defining problem; see log.');
    }
  }


  setAmount(e) {
    console.log('Setting amount: ' + e.target.value);
    this.model.amount = e.target.value;
  }

  setReceiver(e) {
    console.log('Setting receiver: ' + e.target.value);
    this.model.receiver = e.target.value;
  }

  ngOnDestroy() {
    console.log("page destroyed");
    if (this.web3Service.accountsObservable.closed)
      this.web3Service.accountsObservable.unsubscribe();
    if (this.web3Service.accountsObservableGanache.closed)
      this.web3Service.accountsObservableGanache.unsubscribe();
  }
}
