import { Component, Inject, OnInit } from '@angular/core';
import { Web3Service } from '../../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
//import { IPFS } from '../../ipfs';
import { Buffer } from 'buffer';

declare let require: any;
const ERC20_artifacts = require('../../../../build/contracts/ERC20.json');
const channel_artifacts = require('../../../../build/contracts/Channel.json');

// const IPFS = require('ipfs');

@Component({
  selector: 'app-problem-detail',
  templateUrl: './problem-detail.component.html',
  styleUrls: ['./problem-detail.component.css']
})
export class ProblemDetailComponent implements OnInit {
  hash: string;

  accounts: string[];
  chosenAccounts: string[];
  ERC20: any;
  Channel: any;
  Size: any;
  problem: any;
  problems: any[] = [];
  username: any = "Cem";
  imgname: any;
  testCSVPath: any = "";

  path: any[] = [0, 2, 0]
  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    proposals: []
  };

  status = '';

  index: any;
  problemType: any;
  problemTSPList: any[] = [];
  problemGCList: any[] = [];
  problemIPList: any[] = [];
  dataList: any[] = [];
  dataRequestList: any[] = [];
  modelList: any[] = [];
  newPath: any;
  newColoring: any;
  newPartition: any;
  requestDataEventNumber = 0;
  sendResultEventNumber = 0;
  decideResultEventNumber = 0;
  sendModelEventNumber = 0;

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar,
    private route: ActivatedRoute, private router: Router, private httpClient: HttpClient
    //,@Inject(IPFS) private ipfs
  ) {
    console.log('Constructor: ' + web3Service);
    this.web3Service.artifactsToContract(ERC20_artifacts)
      .then((ERC20Abstraction) => {
        this.ERC20 = ERC20Abstraction;
      });

    this.imgname = require("../../../panda.png");
  }

  ngOnInit(): void {

    /* 
    ------------- IPFS -------------
    const node = new IPFS();

    let bufferhello = Buffer.from("hello");
    console.log(bufferhello);

    node.on('ready', async () => {
      const version = await node.version()

      console.log('Version:', version.version)

      const filesAdded = await node.add({
        path: 'hello.txt',
        content: Buffer.from('Hello World 101')
      })

      console.log('Added file:', filesAdded[0].path, filesAdded[0].hash)

      const fileBuffer = await node.cat(filesAdded[0].hash)

      console.log('Added file contents:', fileBuffer.toString())
    })
    ------------- IPFS -------------
    */




    console.log('OnInit: ' + this.web3Service);
    console.log(this);

    this.route.params.subscribe(params => {
      console.log(params);
      this.index = params['index'];
      this.problemType = params['problemType'];
    });
    console.log(this.index);

    this.watchAccount();
    this.listAccounts();

    /*this.web3Service.accountsObservable.subscribe((accounts) => {
      console.log(accounts);
      this.model.account = accounts[0];
    });*/

    this.web3Service.artifactsToContract(channel_artifacts)
      .then((ChannelAbstraction) => {
        this.Channel = ChannelAbstraction;
        this.Channel.deployed().then(deployed => {
          console.log(deployed);
          console.log('heyyy');
          this.listPeople();
          deployed.sendTrainingRequestEvent({ filter: { from: this.model.account } }, (err, ev) => {
            console.log('sendTrainingRequestEvent event came in, select accept or reject');
            this.model.proposals.push("here", ev);
          });
          deployed.acceptTrainingRequestEvent({ filter: { to: this.model.account } }, (err, ev) => {
            console.log('acceptTrainingRequestEvent event came in, select accept or reject');
          });
          deployed.defineTSPEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemTSPList.push(ev.returnValues);
            let matrix: any = [];
            let size = this.problemTSPList[ev.returnValues.index].n;
            for (let i = 0; i < size; i = i + 1) {
              let row: any = [];
              for (let j = 0; j < size; j = j + 1) {
                row.push(this.problemTSPList[ev.returnValues.index].matrix[i * size + j]);
              }
              row = row.slice(0, size).join(" ");
              matrix.push(row);
            }
            this.problemTSPList[ev.returnValues.index].matrix = matrix;
            console.log("TSP problem", ev.returnValues);
          });
          deployed.solveTSPEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemTSPList[ev.returnValues.indexfe].path = ev.returnValues.path;
            this.problemTSPList[ev.returnValues.indexfe].len = ev.returnValues.len;
          });

          deployed.defineGraphColEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemGCList.push(ev.returnValues);
            let matrix: any = [];
            let size = this.problemGCList[ev.returnValues.index].instanceSize;
            for (let i = 0; i < size; i = i + 1) {
              let row: any = [];
              for (let j = 0; j < size; j = j + 1) {
                row.push(this.problemGCList[ev.returnValues.index].instance[i * size + j]);
              }
              row = row.slice(0, size).join(" ");
              matrix.push(row);
            }
            this.problemGCList[ev.returnValues.index].instance = matrix;
            console.log("GC problem", ev.returnValues);
          });
          deployed.solveGraphColEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemGCList[ev.returnValues.indexfe].coloring = ev.returnValues.coloring;
            this.problemGCList[ev.returnValues.indexfe].k_min = ev.returnValues.k;
          });

          deployed.defineIntPtnEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.problemIPList.push(ev.returnValues);
            if (this.problemIPList[ev.returnValues.index].numPtn == 1) {
              this.problemIPList[ev.returnValues.index].partition =
                [this.problemIPList[ev.returnValues.index].partition.join(" ")];
            }
            else {
              let partition: any = [];
              let sum = 0;
              let partitionString: any = "";
              ev.returnValues.partition.forEach(element => {
                sum = sum + parseInt(element);
                if (sum != this.problemIPList[ev.returnValues.index].instance) {
                  partitionString = partitionString + element + " ";
                }
                else { //(sum == this.problemIPList[ev.returnValues.indexfe].instance) 
                  partitionString = partitionString + element;
                  partition.push(partitionString);
                  sum = 0;
                  partitionString = "";
                }
              });
              console.log(partition);
              this.problemIPList[ev.returnValues.index].partition = partition;
              this.problemIPList[ev.returnValues.index].indices = ev.returnValues.indices;
              this.problemIPList[ev.returnValues.index].numPtn = ev.returnValues.numPtn;
            }
            console.log("IP problem", ev.returnValues);
          });
          /*deployed.getPartition.call(this.model.account, this.index, (err, ev) => {
            console.log(ev);
            this.tempPtn = ev.returnValues;
          });*/
          deployed.solveIntPtnEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            //this.problemIPList[ev.returnValues.indexfe].partition = this.tempPtn;
            let partition: any = [];
            let sum = 0;
            let partitionString: any = "";
            ev.returnValues.partition.forEach(element => {
              sum = sum + parseInt(element);
              if (sum != this.problemIPList[ev.returnValues.indexfe].instance) {
                partitionString = partitionString + element + " ";
              }
              else { //(sum == this.problemIPList[ev.returnValues.indexfe].instance) 
                partitionString = partitionString + element;
                partition.push(partitionString);
                sum = 0;
                partitionString = "";
              }
            });
            console.log(partition);
            this.problemIPList[ev.returnValues.indexfe].partition = partition;
            this.problemIPList[ev.returnValues.indexfe].indices = ev.returnValues.indices;
            this.problemIPList[ev.returnValues.indexfe].numPtn = ev.returnValues.numPtn;
          });

          deployed.defineDataEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            this.dataList.push(ev.returnValues);
            console.log("Data", ev.returnValues);
          });
          //console.log("model account", this.model.account)
          deployed.requestDataEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            console.log("requestDataEvent", ev.returnValues);
            console.log("account", this.model.account);
            console.log("num", this.requestDataEventNumber);

            if (ev.returnValues.to == this.model.account) {
              this.requestDataEventNumber++;
              this.dataRequestList.push(ev.returnValues);
              console.log("Data Request", ev.returnValues);
              let dataJSON = { "data": ev.returnValues }
              this.httpClient.post('http://127.0.0.1:5001/requestTraining', dataJSON).subscribe((response: any) => {
                this.trainingCompleted(response);
              });
            }
          });
          deployed.sendResultEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            console.log("sendResultEvent", ev.returnValues);
            console.log("account", this.model.account);
            console.log("num", this.sendResultEventNumber);
            if (ev.returnValues.to == this.model.account) {
              this.modelList.push(ev.returnValues);
              console.log("hashed model sent", ev.returnValues);
              let dataJSON = { "data": ev.returnValues }
              this.httpClient.post('http://127.0.0.1:5000/getResults', dataJSON).subscribe((response: any) => {
                this.accuracyChecked(response);
              });
            }
          });
          deployed.decideResultEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            console.log("decideResultEvent", ev.returnValues);
            console.log("account", this.model.account);
            console.log("num", this.decideResultEventNumber);
            if (ev.returnValues.to == this.model.account) {
              console.log("results are checked", ev.returnValues);
              let dataJSON = { "data": ev.returnValues }
              this.httpClient.post('http://127.0.0.1:5001/sendModel', dataJSON).subscribe((response: any) => {
                this.modelSent(response);
              });
            }
          });
          deployed.sendModelEvent({
            fromBlock: 0,
            toBlock: this.web3Service.getBlockNumber()
          }, (err, ev) => {
            console.log("sendModelEvent", ev.returnValues);
            console.log("account", this.model.account);
            console.log("num", this.sendModelEventNumber);
            if (ev.returnValues.to == this.model.account) {
              console.log("model is received", ev.returnValues);
              let dataJSON = { "data": ev.returnValues }
              this.httpClient.post('http://127.0.0.1:5000/getModel', dataJSON).subscribe((response: any) => {
                this.modelChecked(response);
              });
            }
          });
        });
      });

    console.log(this.problemTSPList);
  }

  async modelChecked(response) {
    console.log("model check done");
    console.log(response);
    if (!this.Channel || response['dataOwner'] == "dummy") {
      return;
    }

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });
      const deployedERC20 = await this.ERC20.deployed();
      const deployedChannel = await this.Channel.deployed();
      const requestData = await deployedChannel.decideModel(response['dataOwner'], response['decision'], parseInt(this.dataList[this.index].price), deployedERC20.address, { from: addr });

      console.log(requestData);
      if (!requestData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }
  
  async modelSent(response) {
    console.log("model sending done");
    console.log(response);
    if (!this.Channel || response['testOwner'] == "dummy") {
      return;
    }

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const requestData = await deployedChannel.sendModel(response['testOwner'], { from: addr });

      console.log(requestData);
      if (!requestData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  async accuracyChecked(response) {
    console.log("accuracy check done");
    console.log(response);
    if (!this.Channel || response['dataOwner'] == "dummy") {
      return;
    }

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const requestData = await deployedChannel.decideResult(response['dataOwner'], response['decision'], { from: addr });

      console.log(requestData);
      if (!requestData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  async trainingCompleted(response) {
    console.log("training completed");
    console.log(response);
    if (!this.Channel || response['testOwner'] == "dummy") {
      return;
    }

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const requestData = await deployedChannel.sendResult(response['testOwner'], response['hashedModel'], response['hashedResults'], { from: addr });

      console.log(requestData);
      if (!requestData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  watchAccount() {
    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.accounts = accounts;
        this.model.account = accounts[0];
        //        this.refreshBalance();

      });
    }
    catch (e) {
      console.log(e);
    }
  }

  listAccounts() {
    console.log('list');
    this.web3Service.accountsObservableGanache.subscribe((accounts) => {
      console.log('accounts');
      console.log(accounts);
      this.chosenAccounts = accounts;
      this.model.receiver = accounts[0];
      //      this.refreshBalance();

    })
  }

  public getName(name) {
    console.log(this.web3Service.asciiToString(name));
  }


  async listPeople() {
    try {
      let person;
      const deployedChannel = await this.Channel.deployed();
      deployedChannel.people(this.model.account).then((person) => {
        console.log(person);
        this.getName(person.name);
      });
    }
    catch (e) {
      console.log(e);
    }
  }

  setStatus(status) {
    this.matSnackBar.open(status, null, { duration: 3000 });
  }

  // public refreshBalance() {
  //   console.log('Refreshing balance');
  //   try {
  //     this.ERC20.deployed().then(deployedERC20 => {
  //       deployedERC20.getBalance.call(this.model.account).then(metaCoinBalance => {
  //         console.log('Found balance: ' + metaCoinBalance);
  //         this.model.balance = metaCoinBalance;
  //       });
  //     });
  //   } catch (e) {
  //     console.log(e);
  //     this.setStatus('Error getting balance; see log.');
  //   }
  // }


  async submitTSPSolution() {
    if (!this.Channel) {
      return;
    }

    console.log("Submit Solution button clicked");

    let pathArray: any[] = [];
    this.newPath.split(/\s+/).forEach(a => {
      pathArray.push(parseInt(a));
    });

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedERC20 = await this.ERC20.deployed();
      const deployedChannel = await this.Channel.deployed();
      const solveProblem = await deployedChannel.sendPath.sendTransaction(this.problemTSPList[this.index].from, this.index,
        this.problemTSPList[this.index].n, pathArray, deployedERC20.address, this.index, { from: addr });

      console.log(solveProblem);
      if (!solveProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  async submitGCSolution() {
    if (!this.Channel) {
      return;
    }

    console.log("Submit Solution button clicked");

    let coloringArray: any[] = [];
    this.newColoring.split(/\s+/).forEach(a => {
      coloringArray.push(parseInt(a));
    });

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedERC20 = await this.ERC20.deployed();
      const deployedChannel = await this.Channel.deployed();
      const solveProblem = await deployedChannel.sendColoring.sendTransaction(this.problemGCList[this.index].from, this.index,
        this.problemGCList[this.index].instanceSize, coloringArray, deployedERC20.address, this.index, { from: addr });

      console.log(solveProblem);
      if (!solveProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  async submitIPSolution() {
    if (!this.Channel) {
      return;
    }

    console.log("Submit Solution button clicked");


    var ind = 0;
    let partitionArray: any[] = [];
    let indicesArray: any[] = [];

    this.newPartition.split("\n").forEach(ptn => {
      indicesArray.push(ind);
      var ctr = 0;
      ptn.split(/\s+/).forEach(part => {
        partitionArray.push(parseInt(part));
        ctr++;
      })
      ind += ctr;
    });
    indicesArray.push(partitionArray.length);


    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedERC20 = await this.ERC20.deployed();
      const deployedChannel = await this.Channel.deployed();
      const solveProblem = await deployedChannel.sendPartition.sendTransaction(this.problemIPList[this.index].from, this.index,
        partitionArray, indicesArray, deployedERC20.address, this.index, { from: addr });

      console.log(solveProblem);
      if (!solveProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  dataRequest() {
    let testCSVPathJSON = { "path": this.testCSVPath }
    let numRows: any;
    let numColumns: any;
    let testExpResultString: any;
    let testCipherResult: any;
    let testCipherTempResult: any;
    let testExpResult: any;
    let testExpString: any;
    let testCipher: any;
    let testCipherTemp: any;
    let testExp: any;
    let pk1g: any;
    let pk1n: any;
    this.httpClient.post('http://127.0.0.1:5000/client', testCSVPathJSON).subscribe((response: any) => {
      console.log(response);

      numRows = response['hashedTest'].length;
      numColumns = response['hashedTest'][0].length;
      console.log(numRows);
      console.log(numColumns);

      testCipherTemp = [];
      testExp = [];

      response['hashedTest'].forEach(row => {
        row.forEach(cell => {
          testCipherTemp.push(cell[0]);
          testExp.push(cell[1]);
        });
      });

      testCipher = "";
      testCipherTemp.forEach(elt => {
        testCipher += (elt + ",");
      });

      testExpString = "";
      testExp.forEach(elt => {
        testExpString += (elt + ",");
      });

      testCipherTempResult = [];
      testExpResult = [];

      response['hashedTestResults'].forEach(row => {
        testCipherTempResult.push(row[0]);
        testExpResult.push(row[1]);
      });

      testCipherResult = "";
      testCipherTempResult.forEach(elt => {
        testCipherResult += (elt + ",");
      });

      testExpResultString = "";
      testExpResult.forEach(elt => {
        testExpResultString += (elt + ",");
      });

      pk1g = response['pk1'].g;
      pk1n = response['pk1'].n;


      //console.log("t", testCipher);
      //console.log("tresult", testCipherResult);
      //console.log("texp", testExpString);
      //console.log("tresultexp", testExpResultString);
      //console.log(testCipher);
      //console.log(testExp);
      this.sendDataRequest(numRows, numColumns, testCipher, testExpString, testCipherResult, testExpResultString, pk1g, pk1n);

    });

  }

  async sendDataRequest(numRows, numColumns, testCipher, testExpString, testCipherResult, testExpResultString, pk1g, pk1n) {
    if (!this.Channel) {
      return;
    }

    try {
      let addr: any;
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        addr = accounts[0];
      });

      const deployedERC20 = await this.ERC20.deployed();
      const deployedChannel = await this.Channel.deployed();
      const requestData = await deployedChannel.requestData.sendTransaction(this.dataList[this.index].from, numRows,
        numColumns, testCipher, testExpString, testCipherResult, testExpResultString, pk1g, pk1n, { from: addr });

      console.log(requestData);
      if (!requestData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  setAmount(e) {
    console.log('Setting amount: ' + e.target.value);
    this.model.amount = e.target.value;
  }

  setReceiver(e) {
    console.log('Setting receiver: ' + e.target.value);
    this.model.receiver = e.target.value;
  }

  ngOnDestroy() {
    console.log("page destroyed");
    if (this.web3Service.accountsObservable.closed)
      this.web3Service.accountsObservable.unsubscribe();
    if (this.web3Service.accountsObservableGanache.closed)
      this.web3Service.accountsObservableGanache.unsubscribe();
  }
}
