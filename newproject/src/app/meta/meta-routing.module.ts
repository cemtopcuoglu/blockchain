import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MetaSenderComponent } from './meta-sender/meta-sender.component';
import { CreateComponent } from './create/create.component';
import { TSPInstanceComponent } from './tsp-instance/tsp-instance.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { ProblemDetailComponent } from './problem-detail/problem-detail.component'

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  {
    path: 'metasender',
    runGuardsAndResolvers: 'always',
    component: MetaSenderComponent

  },
  { path: 'create', component: CreateComponent },
  { path: 'instance', component: TSPInstanceComponent },
  { path: 'user', component: UserInfoComponent },
  { path: 'problemdetail/:problemType/:index', component: ProblemDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MetaRoutingModule { }