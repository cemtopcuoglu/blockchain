import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../util/web3.service';
import { MatSnackBar } from '@angular/material';
import { MetaSenderComponent } from '../meta-sender/meta-sender.component'

declare let require: any;
const channel_artifacts = require('../../../../build/contracts/Channel.json');

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  tsp = {
    size: null,
    basePrice: null,
    rate: null,
    problemMatrixText: '',
    path: [],
    pathText: '',
    problemMatrix: []
  }

  gc = {
    size: null,
    basePrice: null,
    rate: null,
    problemMatrixText: '',
    coloring: [],
    coloringText: '',
    problemMatrix: []
  }

  ip = {
    basePrice: null,
    rate: null,
    problemIntText: '',
    partition: null,
    indices: null,
    partitionText: '',
    problemInt: null
  }

  data = {
    name: null,
    price: null,
    description: '',
    header: ''
    //,splittedHeader: []
  }

  problemType: null;
  account: any;
  Channel: any;

  constructor(private web3Service: Web3Service, private matSnackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.web3Service.artifactsToContract(channel_artifacts)
      .then((ChannelAbstraction) => {
        this.Channel = ChannelAbstraction;
      });
  }

  createNewTSP() {
    console.log('Creating TSP instance');
    console.log('problem', this.tsp);

    let temp = this.tsp.problemMatrixText.split(/\s+/);
    this.tsp.size = temp[0];

    temp.shift();

    for (let j = 0; j < this.tsp.size * this.tsp.size; j++) {
      this.tsp.problemMatrix[j] = parseInt((temp[j]));
    }

    this.tsp.pathText.split(/\s+/).forEach(a => {
      this.tsp.path.push(parseInt(a));
    });

    if (isNaN(this.tsp.path[0])) {
      this.tsp.path = [];
    }

    console.log('matrix', this.tsp.problemMatrix);

    this.submitTSP();
  }

  async submitTSP() {
    if (!this.Channel) {
      return;
    }

    console.log('Submitting TSP');

    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.account = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const defineProblem = await deployedChannel.defineTSP.sendTransaction(this.tsp.problemMatrix, this.tsp.size, this.tsp.path, this.tsp.basePrice, this.tsp.rate, { from: this.account });

      console.log(defineProblem);
      if (!defineProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  createNewGC() {
    console.log('Creating GC instance');
    console.log('problem', this.gc);

    let temp = this.gc.problemMatrixText.split(/\s+/);
    this.gc.size = temp[0];

    temp.shift();

    for (let j = 0; j < this.gc.size * this.gc.size; j++) {
      this.gc.problemMatrix[j] = parseInt((temp[j]));
    }

    this.gc.coloringText.split(/\s+/).forEach(a => {
      this.gc.coloring.push(parseInt(a));
    });

    if (isNaN(this.gc.coloring[0])) {
      this.gc.coloring = [];
    }

    console.log('matrix', this.gc.problemMatrix);

    this.submitGC();
  }

  async submitGC() {
    if (!this.Channel) {
      return;
    }

    console.log('Submitting GC');

    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.account = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const defineProblem = await deployedChannel.defineGraphCol.sendTransaction(this.gc.problemMatrix, this.gc.size, this.gc.coloring, this.gc.basePrice, this.gc.rate, { from: this.account });

      console.log(defineProblem);
      if (!defineProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  createNewIP() {
    console.log('Creating IP instance');
    console.log('problem', this.ip);

    let temp = this.ip.problemIntText;
    this.ip.problemInt = parseInt(temp);

    //console.log("1", this.ip.partitionText);
    /*var i = 0;
    this.ip.partition = [];
    if (this.ip.partitionText != "") {
      this.ip.partitionText.split("\n").forEach(ptn => {
        this.ip.partition.push([]);
        ptn.split(/\s+/).forEach(part => {
          this.ip.partition[i].push(parseInt(part));
        })
        i++;
      });
    }*/
    /*console.log("2", this.ip.partition);
    console.log(typeof this.ip.partition);
    console.log(typeof this.ip.partition[0]);
    console.log(typeof this.ip.partition[0][0]);*/

    var ind = 0;
    this.ip.partition = [];
    this.ip.indices = [];

    if (this.ip.partitionText != "") {
      this.ip.partitionText.split("\n").forEach(ptn => {
        this.ip.indices.push(ind);
        var ctr = 0;
        ptn.split(/\s+/).forEach(part => {
          this.ip.partition.push(parseInt(part));
          ctr++;
        })
        ind += ctr;
      });
    }
    this.ip.indices.push(this.ip.partition.length);

    console.log('PARTITION', this.ip.partition);
    console.log('INDICES', this.ip.indices);

    console.log('integer', this.ip.problemInt);

    this.submitIP();
  }

  async submitIP() {
    if (!this.Channel) {
      return;
    }

    console.log('Submitting IP');
    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.account = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const defineProblem = await deployedChannel.defineIntPtn.sendTransaction(this.ip.problemInt, this.ip.partition, this.ip.indices, this.ip.basePrice, this.ip.rate, { from: this.account });

      console.log(defineProblem);
      if (!defineProblem) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }

  createNewData() {
    //this.data.splittedHeader = this.data.header.split(/\s+/);
    //console.log(this.data.splittedHeader);
    this.submitData();
  }

  async submitData() {
    if (!this.Channel) {
      return;
    }

    console.log('Submitting Data');
    try {
      this.web3Service.accountsObservable.subscribe((accounts) => {
        console.log(accounts);
        this.account = accounts[0];
      });

      const deployedChannel = await this.Channel.deployed();
      const defineData = await deployedChannel.defineData.sendTransaction(this.data.name, this.data.price, this.data.description, this.data.header, { from: this.account });

      //console.log("AAAAAAAAAAAAAAAAA");
      console.log(defineData);
      if (!defineData) {
        //     this.setStatus('define problem failed!');
      } else {
        //     this.setStatus('define problem complete!');
      }
    } catch (e) {
      console.log(e);
    }
  }
}